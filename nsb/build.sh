#!/bin/bash

#  build.sh
#  nfobuilder
#
#  Created by Kiss Ferenc on 2014.07.28..
#

nsbtarfolder="$USER_APPS_DIR/nsb"
if [[ -d "$nsbtarfolder" ]];then
	rm -r "$nsbtarfolder"
fi

mkdir "$nsbtarfolder"

nsbsrc="$SRCROOT/nsb.sh"
nsbtar="$USER_APPS_DIR/nsb/nsb.sh"

cp "$nsbsrc" "$nsbtar"
chmod +x "$nsbsrc"
chmod +x "$nsbtar"

cd "$SRCROOT"
echo "dir: $PWD"

funcsrc="functions"
functar="$USER_APPS_DIR/nsb/functions"
cp -R "$funcsrc" "$functar"

targetname="testresult.nfo"
targetnsb="sprites/$targetname"
grftest="testresult.grf"

eval $nsbsrc "test.txt" "$targetnsb"
returncode=$?

#exit 0

mehet=false
case $returncode in
0)
	echo "NSB sikeresen lefutott"
	mehet=true
;;
1)
	echo "NSB figyelmeztetéssel lefutott"
	mehet=true

;;
2)
echo "NSB hiba"
;;
*)
echo "ismeretlenw kimenet: $returncode"
;;
esac

if [[ $mehet == true ]]; then
	eval "nforenum $targetname 2>&1"
	returncode=$?
	if [ $returncode == 0 ]; then
		echo "NFRRENUM sikeresen lefutott"
		eval "grfcodec -e $grftest 2>&1"
		returncode=$?
		if [ $returncode == 0 ]; then
			echo "GRFCODEC sikeresen lefutott"
		fi
	fi
fi

exit $returncode
