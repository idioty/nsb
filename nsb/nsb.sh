#!/bin/bash

#  nsb.sh
#  nsb
#
#  Created by Kiss Ferenc on 2014.07.28..
#

#[speed] ha gond lenne valami ekezetessel, ez kikommentelheto, de igy ad +10% speed-et!
LC_ALL=C
shopt -s extglob
spriteidsmy=vackor
# mostani konyvtar megvaltoztatasa a futtathato fajl konyvtarara
scriptdirname=$(dirname $0)
cd $scriptdirname


# files
source=$1
target=$2

# "celpontot" toroljuk
echo -ne "" > $target


 ###########################################################################################
#																			#
#									OTHER FUNCTIONS							#
#																			#
 ###########################################################################################

debug () {
    echo "[debug] $*" 1>&2
}

checkpoweroftwo() {
	result="true"
	if [[ $1 -lt 2 ]]; then
		result="false"
	else
		binstr=$(echo "obase=2;$1" | bc)
		talalat=0
		for ((i=0; i<"${#binstr}"; i++)); do
			if [[ ${binstr:$i:1} == "1" ]]; then
				((talalat++))
				if [[ $talalat -gt 1 ]]; then
					result="false"
					break
				fi
			fi
		done
	fi

	echo $result
}

checkpowerof2 () {
        case $1 in 2|4|8|16|32|64|128|256|512|1024|2048|4096|8192|16384|32768|65536|131072|262144|524288|1048576|2097152|4194304|8388608|16777216|33554432|67108864|134217728|268435456|536870912|1073741824|2147483648|4294967296)
                   return 0
                ;;
                *) return 1
                ;;
        esac
}

# a trim levagja az elso es utolso whitespaceket
trim() {
	local var=$@
	var="${var#"${var%%[![:space:]]*}"}"   # remove leading whitespace characters
	var="${var%"${var##*[![:space:]]}"}"   # remove trailing whitespace characters
#	var=${var//$'\n'/}	# eltavolitjuk az osszes newline karaktert
	echo -n "$var"
}

# decimal numbert alakitja at hexara
# \param $1: required; maga a szam, amit at kell alakitani
# \param $2: optional; hossza a bytenak. Igazabol a lenyege, hogy levagja az elejetol fogva, ha hosszabb
# \param $3: optional; ha true, akkor forditott byte sorrendben adja vissza

# byte-ok szokozzel valo elvalaszatasa
addspacestobyte() {
	str="$1"
	temp=""
	for ((aspj=0; aspj<"${#str}"; aspj+=2)); do
		temp+="${str:$aspj:2} "
	done
	echo ${temp% }
}

bytereverse() {
	str="$1"
	for ((dthi=0; dthi<${#str}; dthi+=2)); do
		dthtemp="${str:$dthi:2}$dthtemp"
	done
	echo "$dthtemp"
}

dectohex() {
case $2 in
2) echo "\\\b$1";;
4) echo "\w$1";;
8) echo "\\\d$1";;
*)
	if  [[ -n $2 ]]
		then printf -v vissza "%0${2}X\n" $1
		else printf -v vissza "%X\n" $1
	fi
	if [[ -n $3 ]] && $3 ; then
		vissza=$(bytereverse "$vissza")
		if [[ -n $4 ]] && [[ $4 == spaces ]]; then
			vissza=$(addspacestobyte $vissza)
		fi
	fi
	echo ${vissza% }
;;
esac
}
#[speed]
# echo "dectohex: $(dectohex 2 4 true)"

replaceInQuotationMarks2() {
	string="${1//'\"'/<!peridezojel!>}"

	SAVEIFS=$IFS
	IFS=\"

	ar=( ${string} );
	arc="${#ar[@]}"
	result=""

	for ((i=0;i<$arc;i++)) do
		if [[ $((i%2)) -eq 0 ]]; then
			result+="${ar[$i]}"
		else
			temp="${ar[$i]//,/<!vesszo!>}"
			temp="${temp//' '/<!_!>}"
			result+="$temp"
		fi
	done

	IFS=$SAVEIFS
	echo "$result"
}

replaceInQuotationMarks() {
	string="$1"
	chars=${#string}
	result=""
	checknext=0
	inquote=0
	for ((i=0; i<$chars; i++)) do
		char=${string:$i:1}
		if [[ $checknext -eq 0 ]]; then
			if [[ $char == "\"" ]]; then
				if [[ inquote -eq 0 ]]; then
					inqoute=1
				else
					inquote=0
				fi
				#result+="$char" # nem adjuk hozza, mert akkor meg el kell majd tavolitani...
				continue
			fi
		fi

		if [[ $char == "\\" ]]; then
			checknext=1
			result+="\\"
			continue
		fi

		if [[ $inqoute -eq 1 ]] && [[ $checknext -eq 0 ]]; then
			if [[ $char == ',' ]]; then
				result+="<!vesszo!>"
			elif [[ $char == ' ' ]]; then
				result+="<!_!>"
			else
				result+="$char"
			fi
		else
			result+="$char"
		fi

		checknext=0
	done

	echo "$result"
}

replaceBackCharacters() {
	string=${1//'<!vesszo!>'/,};
	string=${string//'<!_!>'/ };
	string=${string//'<!peridezojel!>'/'\"'};
	echo "$string"
}


#astring='peldastring, itt nem kell, de "valami benne van, es az jo, a francba, \"tudom, he\", nemcsere: \"nemcsere\"", ezt itt megint nem kell cserelni, "de ezt, mar igen"'
#valaki=$(replaceInQuotationMarks2 "$astring")
#echo "valaki: $valaki"
#valaki=$(replaceBackCharacters "$valaki")
#echo "valaki2: $valaki"


# checkings
szam () {
    [[ $1 == ?(-)+([0-9]) ]]
}

pszam () {
    [[ $1 == +([0-9]) ]]
}

szam8 () {
    (($1>=0 && $1<=255)) 2>/dev/null
}

szam8n () {
    (($1>=-128 && $1<=127)) 2>/dev/null
}

szam16 () {
    (($1>=0 && $1<=65535)) 2>/dev/null
}
szam161 () {
    (($1>=1 && $1<=65535)) 2>/dev/null
}

szam16n () {
    (($1>=-32768 && $1<=32767)) 2>/dev/null
}

szam32 () {
    (($1>=0 && $1<=4294967295)) 2>/dev/null
}

hex () {
	if [[ -z $2 ]]; then
		[[ $1 =~ ^\((hex|rhex)\)([0-9a-fA-F][0-9a-fA-F])*$ ]]
	else
		aabytes=$(($2 * 2))
		[[ $1 =~ ^\((hex|rhex)\)([0-9a-fA-F]){$aabytes}$ ]]
	fi
}

# azaz eltavolitjuk a hex vagy rhex elotagokat,
# valamint alapbol hozzaadjuk a spaceket,
# es megforditjuk alapbol vagy nem, ha rhex
hexraw () {
	result=$1
	if [[ $result =~ ^\(hex\).* ]]; then
		result=${result//(hex)/}
		bytes=${#result}
		case $bytes in
			2) result="\\\bx$result" ;;
			4) result="\wx$result" ;;
			8) result="\\\dx$result" ;;
		esac
	else
		if [[ $result =~ ^\(rhex\).* ]]; then
			result=${result//(rhex)/}
			result=$(addspacestobyte $result)
		fi
	fi
	echo $result
}

hexremove () {
	result="${1//(rhex)/}"
	result="${result//(hex)/}"
	echo "$result"
}


#test=(rhex)34eda3
#test=(rhex)1400
#test=(hex)14001400
#
#if hex $test 4; then
#echo hexje: $(hexraw $test)
#else
#echo nohex
#fi

stringlength () {
    echo ${#1}
}

error () {
    echo "$*" 1>&2
    error=true
}
warning () {
    echo "$*" 1>&2
    warning=true
}



range_pos=szam16
rangeerror_pos="0 and 65535"
range_size=szam161
rangeerror_size="1 and 65535"
range_rel=szam16n
rangeerror_rel="-32768 and 32767"
boxrange_offset=szam8n
boxrangeerror_offset="-128 and 127"
boxrange_size=szam8
boxrangeerror_size="0 and 255"

 ###########################################################################################
#																			#
#									BUILDER FUNCTIONS							#
#																			#
 ###########################################################################################

source ./functions/ErrorsAndWarnings.sh
source ./functions/AddStation.sh
source ./functions/AddSprite.sh
source ./functions/AddLayout.sh
source ./functions/AddLayoutSettings.sh
source ./functions/AddLayoutBoundingbox.sh
source ./functions/AddLayoutRealSprite.sh
source ./functions/AssignGrapghicsSets.sh
source ./functions/StationNames.sh
source ./functions/AddAction2.sh
source ./functions/AddVariationalAction2.sh
source ./functions/AddRandomAction2.sh
source ./functions/AssignSpriteIDToAction2ID.sh
source ./functions/AssignOneGraphicsSetForAllCargo.sh
source ./functions/AddLittleLotsSets.sh

 ###########################################################################################
#																	 		#
#									MAIN PROGRAM								#
#																			#
 ###########################################################################################

# kiolvassuk soronkent
infunction=false
infunctionlines=""
function=""
beginlinecount=""
linecount=0
exitcode=0
action2autoid=0

# ide be kell jegyezni az osszes funkcio osszes lehetseges veget
allallowedclosing=("@EndAddStation" "@EndAddLayout1" "@EndAddLayout2" "@EndAddSprite" "@EndAssignGrapghicsSets" "@EndStationNames" "@EndAddAction2" "@EndAddVariationalAction2" "@EndAddRandomAction2")

SAVEIFS=$IFS
#IFS="
#"
#for line in `cat $source| sed -e 's/^\s*//' -e 's/\s*$//' `
#[speed]
while IFS= read -r line
do
#	saveline=$line
#	echo line: $line
	((linecount++))

#	if [[ ${line::1} == [%/] ]]
#	    then withoutcommentedline=${line/'#'*/}
#		 withoutcommentedline=${withoutcommentedline/\/\/*/}
#	    else withoutcommentedline=$line
#	fi
#	withoutcommentedline=${withoutcommentedline//[[:space:]]/}
#ez itt tok felesleges. kesobb is van erre check

	withoutcommentedline="$line"

	if $infunction; then
		if [[ $withoutcommentedline == @End* ]]; then
#			echo sor: $withoutcommentedline 1>&2
#			echo end function assign: $function

			# funkciok szerinti levalogatas
			#
			# FIGYELEM: a fő (itt bejegyzett) funkcióknak a visszatérő értékei sorrendben:
			#			- action2autoid:	hogy tudjuk hol tartott az action2 automatikus kiosztása
			#

			case $function in
			    AddStation*)
					allowedcolsing=("@EndAddStation" "@EndAddLayout1" "@EndAddLayout2" "@EndAddSprite")
			    ;;
			    AssignGrapghicsSets*)
					allowedcolsing=("@EndAssignGrapghicsSets")
			    ;;
			    StationName*)
					allowedcolsing=("@EndStationNames")
			    ;;
			    AddAction2*)
					allowedcolsing=("@EndAddAction2" "@EndAddVariationalAction2" "@EndAddRandomAction2")
			    ;;
			esac

			if [[ -z $endfunction ]]; then
				echo -e "$source:$beginlinecount: error: Invalid function: $function!\n" 1>&2
				exitcode=2
			else
				if ! [[ ${allowedcolsing[*]} =~ "$withoutcommentedline" ]]; then
					if [[ ${allallowedclosing[*]} =~ "$withoutcommentedline" ]]; then
						echo -e "$source:$linecount: error: Close $functionname function with '$endfunction' before close other function ($withoutcommentedline)!\n" 1>&2
						exitcode=2
					else
						echo -e "$source:$linecount: warning: Invalid command ('$withoutcommentedline'), this line will be ignored!\n" 1>&2
						if [[ $exitcode -lt 1 ]]; then
							exitcode=1
						fi
					fi
				else
					if [[ "$withoutcommentedline" == "$endfunction" ]]; then
						debug "$functionname parameterek: $parameters"
						a2aid=$($functionname "$infunctionlines" "$beginlinecount" "$parameters" "$action2autoid")
						aexitcode=$?
#						echo "axitcode: $aexitcode"
						action2autoid=$a2aid
						if [[ $exitcode -lt $aexitcode ]]; then
							exitcode=$aexitcode
						fi

						infunction=false
						beginlinecount=""
					else
						infunctionlines+=$line$'\n'
					fi
				fi
			fi

		else
			infunctionlines+="$line"$'\n'
		fi
	else
		if [[ ${withoutcommentedline:0:1} = @ ]]; then
			function=${line:1}
			functionname=${function%%:*}
			egysoros=0
			parameters=${function#*:}

			case $function in
				AssignSpriteIDToAction2ID*) egysoros=1 ;;
				AssignOneGraphicsSetForAllCargo*) egysoros=1 ;;
			esac

			if [[ $egysoros -eq 1 ]]; then
				debug "$functionname parameterek: $parameters"
				a2aid=$($functionname "$linecount" "$parameters" "$action2autoid")
				aexitcode=$?
#				echo "axitcode: $aexitcode"
				action2autoid=$a2aid
				if [[ $exitcode -lt $aexitcode ]]; then
					exitcode=$aexitcode
				fi
			else
				infunction=true
				infunctionlines=""
				beginlinecount=$linecount
				endfunction="@End$functionname"
#				echo begin function assign: $function
			fi
		else
			echo "$line" >> "$target"
		fi
	fi
#wdone < <(grep '' "$source")
done < "$source"
IFS=$SAVEIFS

#echo "___ *** exitcode: $exitcode"
exit $exitcode
