#!/bin/bash

#  AddLayoutRealSprite.sh
#  nsb
#
#  Created by Kiss Ferenc on 2014.08.02..
#  Copyright (c) 2014 idioty. All rights reserved.

AddLayoutRealSprite() {
#	RealSprite: posx: 0, posy: 0, sizex: 0, sizey: 0, relx: 0, rely: 0
	error=false
	warning=false

	addlayoutrealspriteparameter="$1"
	addlayoutrealspritelinecount="$2"
	addlayoutrealspritebeginlinecount="$2"

#hihi! :->
	if [[ $addlayoutrealspriteparameter == posx:+([0-9]),posy:+([0-9]),sizex:+([0-9]),sizey:+([0-9]),relx:?(-)+([0-9]),rely:?(-)+([0-9]) ]]; then
		read posx posy sizex sizey relx rely <<<${addlayoutrealspriteparameter//[a-z:,]/ }
	else
		invalidaddlayoutrealspriteconfigurationtext="$source:$addlayoutrealspritebeginlinecount: error: Invalid configuration at RealSprite property!"

		addlayoutrealspriteparameterlines=( ${addlayoutrealspriteparameter//,/ } );
		for addlayoutrealspriteparameterline in ${addlayoutrealspriteparameterlines[*]}; do
			if [ ! "${addlayoutrealspriteparameterline//[^:]/}" = ":" ]; then
				error "$invalidaddlayoutrealspriteconfigurationtext" 
			else
				key=${addlayoutrealspriteparameterline%:*}
				addlayoutrealspritevalue=${addlayoutrealspriteparameterline##*:}
				if [[ $key =~ (posx|posy|sizex|sizey|relx|rely) ]]; then
					if [[ -n ${!key} ]]; then
						warning "$source:$addlayoutrealspritebeginlinecount: warning: Multiple configuration found for '$key' at RealSprite property!"
					fi
					eval "$key"=$addlayoutrealspritevalue
				else	warning "$source:$addlayoutrealspritebeginlinecount: warning: Unknown configuration ('$key') found at RealSprite property!"
				fi
			fi
		done
	fi

	for key in posx posy sizex sizey relx rely; do
		if [[ -z ${!key} ]]; then
			error "$source:$addlayoutrealspritebeginlinecount: error: '$key' param is required, but not defined at RealSprite property!"
		else	eval rc=range_${key%?}
			if ! ${!rc} ${!key} ; then
				eval re=rangeerror_${key%?}
				error "$source:$addlayoutrealspritebeginlinecount: error: Invalid value: '${!key}' found for key: '$key' at RealSprite property! Allowed only integer value between ${!re}!"
			fi
		fi
	done
	echo "$posx $posy $sizex $sizey $relx $rely"


	if $error ; then returncode=2
	elif $warning ; then returncode=1
	else returncode=0
	fi

	return $returncode
}
