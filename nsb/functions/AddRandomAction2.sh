#!/bin/bash

#  AddSprite.sh
#  nsb
#
#  Created by Kiss Ferenc on 2014.07.29..
#  Copyright (c) 2014 idioty. All rights reserved.

AddRandomAction2() {
	error=false
	warning=false

	functionname="AddRandomAction2"
	functionlinecount="$2"
	functionbeginlinecount="$2"

	inaddlayoutfunction=false
	inaddlayoutfunctionlines=""
	inaddlayoutbeginlinecount=0
	inaddlayoutlineparameter=""

	randomtrigger=""
	randomtriggera=""
	randbit=""
	randbita=""

	declare -a addranges
	rangecount=0
	bytelength=0

	bufferlines=""
	rangeslines=""

	randomtrigger="00"
	randbit="10"

	invalidspriteconfigurationtext="$source:$functionbeginlinecount: error: Invalid configuration at $functionname function!"

	functionparameterlines=( ${3//[[:space:]]/} );
	functionparameterlines=( ${functionparameterlines//,/ } );
#	debog "functionparameterlines: $functionparameterlines"

	imax=${#functionparameterlines[@]}
	for ((i=0; i<$imax; i++)); do
		functionparameterline="${functionparameterlines[$i]}"
#		debug "functionparameterline: $functionparameterline"
		functionkey=${functionparameterline%:*}
		functionvalue=${functionparameterline##*:}

#		debug "functionkey: $functionkey, functionvalue: $functionvalue"

		if [[ functionkey == functionname ]]; then
			if [[ "${functionparams//[^:]/}" = ":" ]]; then
				error "$invalidspriteconfigurationtext"
			else
				if [[ -z $functionkey ]]; then
					error "$invalidspriteconfigurationtext"
				else case $functionkey in
	#			    if [[ $functionkey == randomtrigger ]]; then
					randomtrigger)
						if [[ -n $randomtriggera ]]; then
							warning "$source:$functionbeginlinecount: warning: Multiple configuration found for 'randomtrigger' at $functionname function!"
						fi

						randomtrigger=$functionvalue
						randomtriggera="van"
					;;

					randbit)
						((randbitredefinedcount++))
						if [[ -n $randbita ]]; then
							warning "$source:$functionbeginlinecount: warning: Multiple configuration found for 'randbit' at $functionname function!"
						fi

						randbit=$functionvalue
						randbita="van"
					;;

					*)
						warning "$source:$functionbeginlinecount: warning: Unknown configuration ('$functionkey') found at $functionname function!"
					;;
					esac
				fi
			fi
		fi
	done

	while read -r functionline
	do
#		debug "functionline: $functionline"
		((functionlinecount++))

		propertykey=${functionline}
		propertykey=${propertykey/'#'*/}
		propertykey=${propertykey/\/\/*/}
		propertykey=${propertykey//[[:space:]]/}
		propertyvalue=${propertykey#*:}

#		debug "propertykey: $propertykey, propertyvalue: $propertyvalue"

		if [[ $propertykey != @('//'*|'#'*|'') ]]; then
			case $propertykey in
				AddRandom:*)
					paramline=( ${propertyvalue//,/ } );
					((rangecount++))
					paramaction2id=""
					paramcount=1

					for params in ${paramline[*]}; do
						keyvalues=( ${params//:/ } );
						paramkey=${keyvalues[0]}
						paramvalue=${keyvalues[1]}
#						debug "paramkey: $paramkey, paramvalue: $paramvalue"

						if [ ! "${params//[^:]/}" = ":" ]; then
							error "$invalidconfigurationtext"
						else
#							debug "*** MEHET ***: $paramkey"
							case $paramkey in
								count)
#									debug "count :$paramvalue"
									# TODO: count csak szam lehet, talan 0-65535 kozott, vagyis nyilvan kevesebb....
									paramcount="$paramvalue"
									((rangecount+=$paramvalue-1))
								;;

								action2id)
#									debug "action2id :$paramvalue"
									paramaction2id="$paramvalue"
								;;

								*)
									warning "$source:$functionlinecount: warning: Unknown configuration key: '$paramkey' found at AddRandom property!"
								;;
							esac
						fi
					done

					if [[ -z $paramaction2id ]]; then
						error "$source:$functionlinecount: error: 'action2id' property is required, but not defined in AddRandom function!"
					else
#						aaction2id=$(dectohex $paramaction2id 4 true spaces)
						if hex $paramaction2id 2; then
							paramaction2id=$(hexraw $paramaction2id)
						elif szam16 $paramaction2id; then
							paramaction2id=$(dectohex $paramaction2id 4)
						else
							error ""
						fi
						aaction2id=$paramaction2id

						a2id=""
						for ((i=0; i<$paramcount; i++)); do
							a2id+="$aaction2id"
							if [[ $i -lt $paramcount-1 ]]; then
								a2id+="\t"
							fi
						done
						rangeslines+="\t\t\t$a2id\n"
					fi
				;;
			esac
		fi
	done <<< "$1"

	if [[ ${#randomtrigger} -ne 2 ]]; then
		error "$source:$functionbeginlinecount: error: The randomtrigger paramter value must be 2 length!"
	fi

	if [[ ${#randbit} -ne 2 ]]; then
		error "$source:$functionbeginlinecount: error: The randbit paramter value must be 2 length!"
	fi

	if [[ $rangecount -eq 0 ]]; then
		error "$source:$functionbeginlinecount: error: You must last one AddRandom parameter!"
	else
		if checkpowerof2 $rangecount; then
			if [[ "$randbit" == "10" ]] && [[ $rangecount -gt 16 ]]; then
				error "$source:$functionbeginlinecount: error: If randbit is '10' then maximum 16 of AddRandom lines allowed!"
			fi
		else
			error "$source:$functionbeginlinecount: error: You can use only power of two count of AddRandom lines!"
		fi
	fi

#	rc=$(dectohex $rangecount 2 false)
	if hex $rangecount 1; then
		rangecount=$(hexraw $rangecount)
	elif szam8 $rangecount; then
		rangecount=$(dectohex $rangecount 2)
	else
		error ""
	fi
	rc=$rangecount

	echo -n "80 $randomtrigger $randbit\t\t\t//random, randomtrigger, randbit\n\t\t\t$rc\t\t\t\t//range count\n$rangeslines"

	if $error ; then returncode=2
	elif $warning ; then returncode=1
	else returncode=0
	fi

	return $returncode
}
