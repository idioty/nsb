#!/bin/bash

#  ErrorsAndWarnings.sh
#  nsb
#
#  Created by Kiss Ferenc on 2015.01.06..
#  Copyright (c) 2015 idioty. All rights reserved.

InvalidConfigurationError () {
	error "$source:$1: error: Invalid configuration at '$2' function!"
}

#	functionname="AddStation"
#					InvalidConfigurationError $functionlinecount $functionname