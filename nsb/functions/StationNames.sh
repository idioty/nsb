#!/bin/bash

#  StationNames.sh
#  nsb
#
#  Created by Kiss Ferenc on 2014.07.29..
#  Copyright (c) 2014 idioty. All rights reserved.

getlangcode() {

result=''
case $1 in

FF) result='FF' ;; default) result='FF' ;;
00) result='80' ;; 80) result='80' ;; en) result='80' ;; enu) result='80' ;; en-US) result='80' ;;
01) result='81' ;; 81) result='81' ;; eng) result='81' ;; en-GB) result='81' ;;
02) result='82' ;; 82) result='82' ;; de) result='82' ;; de-DE) result='82' ;;
03) result='83' ;; 83) result='83' ;; fr) result='83' ;; fr-FR) result='83' ;;
04) result='84' ;; 84) result='84' ;; es) result='84' ;; spa) result='84' ;; es-ES) result='84' ;;
05) result='85' ;; 85) result='85' ;; eo) result='85' ;; epo) result='85' ;; eo-EO) result='85' ;;
06) result='86' ;; 86) result='86' ;; io) result='86' ;; ido) result='86' ;;
07) result='87' ;; 87) result='87' ;; ru) result='87' ;; rus) result='87' ;; ru-RU) result='87' ;;
08) result='88' ;; 88) result='88' ;; ga) result='88' ;; gle) result='88' ;; en-IE) result='88' ;;
09) result='89' ;; 89) result='89' ;; mt) result='89' ;; mlt) result='89' ;; mt-MT) result='89' ;;
0A) result='8A' ;; 8A) result='8A' ;; ta) result='8A' ;; tam) result='8A' ;; ta-IN) result='8A' ;;
0B) result='8B' ;; 8B) result='8B' ;; cv) result='8B' ;; chv) result='8B' ;;
0C) result='8C' ;; 8C) result='8C' ;; zh) result='8C' ;; zho) result='8C' ;; hant) result='8C' ;; zh-CHT) result='8C' ;;
0D) result='8D' ;; 8D) result='8D' ;; sr) result='8D' ;; srp) result='8D' ;; sr-BA) result='8D' ;; sr-SP) result='8D' ;; Cy-sr-SP) result='8D' ;; Lt-sr-SP) result='8D' ;;
0E) result='8E' ;; 8E) result='8E' ;; nn) result='8E' ;; nno) result='8E' ;; nn-NO) result='8E' ;;
0F) result='8F' ;; 8F) result='8F' ;; cy) result='8F' ;; cym) result='8F' ;;
10) result='90' ;; 90) result='90' ;; be) result='90' ;; bel) result='90' ;; be-BY) result='90' ;;
11) result='91' ;; 91) result='91' ;; mr) result='91' ;; mar) result='91' ;; mr-IN) result='91' ;;
12) result='92' ;; 92) result='92' ;; fo) result='92' ;; fao) result='92' ;; fos) result='92' ;; fo-FO) result='92' ;;
14) result='94' ;; 94) result='94' ;; ar) result='94' ;; ara) result='94' ;; are) result='94' ;; ar-EG) result='94' ;;
15) result='95' ;; 95) result='95' ;; cs) result='95' ;; ces) result='95' ;; cze) result='95' ;; csy) result='95' ;; cs-CZ) result='95' ;;
16) result='96' ;; 96) result='96' ;; sk) result='96' ;; slk) result='96' ;; slo) result='96' ;; sky) result='96' ;; sk-SK) result='96' ;;
18) result='98' ;; 98) result='98' ;; bg) result='98' ;; bul) result='98' ;; bgr) result='98' ;; bg-BG) result='98' ;;
1B) result='9B' ;; 9B) result='9B' ;; af) result='9B' ;; afr) result='9B' ;; afk) result='9B' ;; af-ZA) result='9B' ;;
1E) result='9E' ;; 9E) result='9E' ;; el) result='9E' ;; ell) result='9E' ;; el-GR) result='9E' ;;
1F) result='9F' ;; 9F) result='9F' ;; nl) result='9F' ;; nld) result='9F' ;; dut) result='9F' ;; nlb) result='9F' ;; nl-NL) result='9F' ;; nl-BE) result='9F' ;;
21) result='A1' ;; A1) result='A1' ;; eu) result='A1' ;; bag) result='A1' ;; eus) result='A1' ;; euq) result='A1' ;; eu-ES) result='A1' ;;
22) result='A2' ;; A2) result='A2' ;; ca) result='A2' ;; cat) result='A2' ;; ca-ES) result='A2' ;;
23) result='A3' ;; A3) result='A3' ;; lb) result='A3' ;; ltz) result='A3' ;; frl) result='A3' ;; del) result='2A' ;; fr-LU) result='A3' ;; de-LU) result='A3' ;;
24) result='A4' ;; A4) result='A4' ;; hu) result='A4' ;; hun) result='A4' ;; hu-HU) result='A4' ;;
26) result='A6' ;; A6) result='A6' ;; mk) result='A6' ;; mac) result='A6' ;; mkd) result='A6' ;; mk-MK) result='A6' ;;
27) result='A7' ;; A7) result='A7' ;; it) result='A7' ;; ita) result='A7' ;; it-IT) result='A7' ;;
28) result='A8' ;; A8) result='A8' ;; ro) result='A8' ;; rum) result='A8' ;; ron) result='A8' ;; rom) result='A8' ;; ro-RO) result='A8' ;;
29) result='A9' ;; A9) result='A9' ;; is) result='A9' ;; ice) result='A9' ;; isl) result='A9' ;; is-IS) result='A9' ;;
2A) result='AA' ;; AA) result='AA' ;; lv) result='AA' ;; lav) result='AA' ;; lvi) result='AA' ;; lv-LV) result='AA' ;;
2B) result='AB' ;; AB) result='AB' ;; lt) result='AB' ;; lit) result='AB' ;; lth) result='AB' ;; lt-LT) result='AB' ;;
2C) result='AC' ;; AC) result='AC' ;; sl) result='AC' ;; slv) result='AC' ;; sl-SI) result='AC' ;;
2D) result='AD' ;; AD) result='AD' ;; da) result='AD' ;; dan) result='AD' ;; da-DK) result='AD' ;;
2E) result='AE' ;; AE) result='AE' ;; sv) result='AE' ;; swe) result='AE' ;; sv-SE) result='AE' ;;
2F) result='AF' ;; AF) result='AF' ;; nb) result='AF' ;; nob) result='AF' ;; nb-NO) result='AF' ;;
30) result='B0' ;; B0) result='B0' ;; pl) result='B0' ;; pol) result='B0' ;; plk) result='B0' ;; pl-PL) result='B0' ;;
31) result='B1' ;; B1) result='B1' ;; gl) result='B1' ;; glg) result='B1' ;; gl-ES) result='B1' ;;
32) result='B2' ;; B2) result='B2' ;; fy) result='B2' ;; frs) result='B2' ;; frr) result='B2' ;;
33) result='B3' ;; B3) result='B3' ;; uk) result='B3' ;; ukr) result='B3' ;; uk-UA) result='B3' ;;
34) result='B4' ;; B4) result='B4' ;; et) result='B4' ;; est) result='B4' ;; eti) result='B4' ;; et-EE) result='B4' ;;
35) result='B5' ;; B5) result='B5' ;; 'fi') result='B5' ;; fin) result='B5' ;; 'fi-FI') result='B5' ;;
36) result='B6' ;; B6) result='B6' ;; pt) result='B6' ;; por) result='B6' ;; pt-PT) result='B6' ;;
37) result='B7' ;; B7) result='B7' ;; ptb) result='B7' ;; pt-BR) result='B7' ;;
38) result='B8' ;; B8) result='B8' ;; hr) result='B8' ;; hrv) result='B8' ;; hr-HR) result='B8' ;;
39) result='B9' ;; B9) result='B9' ;; ja) result='B9' ;; jpn) result='B9' ;; ja-JP) result='B9' ;;
3A) result='BA' ;; BA) result='BA' ;; ko) result='BA' ;; kor) result='BA' ;; ko-KR) result='BA' ;;
3C) result='BC' ;; BC) result='BC' ;; ms) result='BC' ;; may) result='BC' ;; msa) result='BC' ;; zlm) result='BC' ;; zsm) result='BC' ;; ms-MY) result='BC' ;;
3D) result='BD' ;; BD) result='BD' ;; aus) result='BD' ;; ena) result='BD' ;; en-AU) result='BD' ;;
3E) result='BE' ;; BE) result='BE' ;; tr) result='BE' ;; tur) result='BE' ;; trk) result='BE' ;; tr-TR) result='BE' ;;
42) result='C2' ;; C2) result='C2' ;; th) result='C2' ;; tha) result='C2' ;; th-TH) result='C2' ;;
54) result='D4' ;; D4) result='D4' ;; vi) result='D4' ;; vie) result='D4' ;; vit) result='D4' ;; vi-VN) result='D4' ;;
56) result='D6' ;; D6) result='D6' ;; hans) result='D6' ;; zh-CHS) result='D6' ;;
5A) result='DA' ;; DA) result='DA' ;; id) result='DA' ;; ind) result='DA' ;; id-ID) result='DA' ;;
5C) result='DC' ;; DC) result='DV' ;; ur) result='DC' ;; urd) result='DC' ;; ur-PK) result='DC' ;;
61) result='E1' ;; E1) result='E1' ;; he) result='E1' ;; heb) result='E1' ;; he-IL) result='E1' ;;
62) result='E2' ;; E2) result='E2' ;; fa) result='E2' ;; fas) result='E2' ;; per) result='E2' ;;

esac

echo -n $result

}

#stationnameaddformat() {
#	result="$1"
#	if [[ "$1" =~ [€‚ƒ„…†‡ˆ‰Š‹ŒŽ‘’“”•–—˜™š›œžŸ¡¢£¤¥¦§¨©ª«¬®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲĳĴĵĶķĸĹĺĻļĽľĿŀŁłŃńŅņŇňŉŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽžſƀƁƂƃƄƅƆƇƈƉƊƋƌƍƎƏƐƑƒƓƔƕƖƗƘƙƚƛƜƝƞƟƠơƢƣƤƥƦƧƨƩƪƫƬƭƮƯưƱƲƳƴƵƶƷƸƹƺƻƼƽƾƿǀǁǂǃǄǅǆǇǈǉǊǋǌǍǎǏǐǑǒǓǔǕǖǗǘǙǚǛǜǝǞǟǠǡǢǣǤǥǦǧǨǩǪǫǬǭǮǯǰǱǲǳǴǵǶǷǸǹǺǻǼǽǾǿȀȁȂȃȄȅȆȇȈȉȊȋȌȍȎȏȐȑȒȓȔȕȖȗȘșȚțȜȝȞȟȠȡȢȣȤȥȦȧȨȩȪȫȬȭȮȯȰȱȲȳȴȵȶȷȸȹȺȻȼȽȾȿɀɁɂɃɄɅɆɇɈɉɊɋɌɍɎɏɐɑɒɓɔɕɖɗɘəɚɛɜɝɞɟɠɡɢɣɤɥɦɧɨɩɪɫɬɭ].* ]]; then
#		result="Þ$1"
#
#	else
#		if [[ "$1" =~ [ɮɯɰɱɲɳɴɵɶɷɸɹɺɻɼɽɾɿʀʁʂʃʄʅʆʇʈʉʊʋʌʍʎʏʐʑʒʓʔʕʖʗʘʙʚʛʜʝʞʟʠʡʢʣʤʥʦʧʨʩʪʫʬʭʮʯʰʱʲʳʴʵʶʷʸʹʺʻʼʽʾʿˀˁ˂˃˄˅ˆˇˈˉˊˋˌˍˎˏːˑ˒˓˔˕˖˗˘˙˚˛˜˝˞˟ˠˡˢˣˤ˥˦˧˨˩˪˫ˬ˭ˮ˯˰˱˲˳˴˵˶˷˸˹˺˻˼˽˾˿̴̵̶̷̸̡̢̧̨̛̖̗̘̙̜̝̞̟̠̣̤̥̦̩̪̫̬̭̮̯̰̱̲̳̹̺̻̼͇͈͉͍͎̀́̂̃̄̅̆̇̈̉̊̋̌̍̎̏̐̑̒̓̔̽̾̿̀́͂̓̈́͆͊͋͌̕̚ͅ͏͓͔͕͖͙͚͐͑͒͗͛ͣͤͥͦͧͨͩͪͫͬͭͮͯ͘͜͟͢͝͞͠͡ͰͱͲͳʹ͵Ͷͷͺͻͼͽ;Ϳ΄΅Ά·ΈΉΊΌΎΏΐΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩΪΫάέήίΰαβγδεζηθικλμνξοπρςστυφχψωϊϋόύώϏϐϑϒϓϔϕϖϗϘϙϚϛϜϝϞϟϠϡϢϣϤϥϦϧϨϩϪϫϬϭϮϯϰϱϲϳϴϵ϶ϷϸϹϺϻϼϽϾϿЀЁЂЃЄЅІЇЈ].* ]]; then
#			result="Þ$1"
#		fi
#	fi
#
#	echo -n $result
#}

StationNames() {
	error=false
	warning=false

	functionname="StationNames"
	functionlinecount="$2"
	afunctionlinecount="$2"
	action2autoid="$4"

	invalidconfigurationtext="$source:$functionlinecount: error: Invalid configuration at $functionname function!"

	setnames=""
	stationnames=""

	while read -r propertykey
	do
		# delete comments, trim
		propertykey=${propertykey/'#'*/}
		propertykey=${propertykey/\/\/*/}
		propertykey=$(replaceInQuotationMarks2 "$propertykey")
#		savedline="$propertykey"
		propertykey=${propertykey//[[:space:]]/}
		propertyvalue=${propertykey#*:}

		((afunctionlinecount++))
#		debug line: $propertykey
		if [[ $propertykey != @('//'*|'#'*|'') ]]; then
			case $propertykey in
				Setname:*)
#					debug "propertyvalue: $propertyvalue"
					paramline=( ${propertyvalue//,/ } );

					stationid=""
					stationidredefinedcount=0
					language=""
					languageredefinedcount=0
					name=""
					nameredefinedcount=0

					for params in ${paramline[*]}; do
						keyvalues=( ${params//:/ } );
						paramkey=${keyvalues[0]}
						paramvalue=${keyvalues[1]}

						if [ ! "${params//[^:]/}" = ":" ]; then
							error "$invalidconfigurationtext"
						else
#							debug "*** MEHET ***"
							case $paramkey in
								stationid)
									if [[ -n $stationid ]] && [[ $stationidredefinedcount -eq 0 ]]; then
										warning "$source:$afunctionlinecount: warning: Multiple configuration found for '$paramkey' at Setname property!"
									fi

									stationid="${paramvalue//_}"

									if ! szam16 $paramvalue ; then
										error "$source:$afunctionlinecount: error: Invalid value: '$paramvalue' found for key: 'stationid' at $functionname function! Allowed only integer values between 0 and 65535!"
									fi
								;;

								language)
									if [[ -n $language ]] && [[ $languageredefinedcount -eq 0 ]]; then
										warning "$source:$afunctionlinecount: warning: Multiple configuration found for '$paramkey' at Setname property!"
									fi

									language=$(getlangcode "${paramvalue//_}")
								;;

								name)
									if [[ -n $name ]] && [[ $nameredefinedcount -eq 0 ]]; then
										warning "$source:$afunctionlinecount: warning: Multiple configuration found for '$paramkey' at Setname property!"
									fi

									name="$paramvalue"
								;;

								*)
									warning "$source:$afunctionlinecount: warning: Unknown configuration key: '$paramkey' found at Setname property!"
								;;
							esac
						fi
 					done

					if [[ -z $stationid ]]; then
						error "$source:$afunctionlinecount: error: 'stationid' param is required, but not defined at Setname property!"
					fi

					if [[ -z $language ]]; then
						error "$source:$afunctionlinecount: error: 'language' param is requierd, but not defined at Setname property or invalid value: '$paramvalue' found for key: 'language' at $functionname function! Allowed iso and hex language ids!"
					fi

					if [[ -z $name ]]; then
						error "$source:$afunctionlinecount: error: 'name' param is required, but not defined at Setname property!"
					fi

					if [[ -z $setnames ]]; then
						setnames="$stationid;$language;$name"
					else
						setnames="$setnames,$stationid;$language;$name"
					fi
				;;

				Station:*)
#					debug "propertyvalue: $propertyvalue"
					paramline=( ${propertyvalue//,/ } );

					stationid=""
					stationidredefinedcount=0
					language=""
					languageredefinedcount=0
					name=""
					nameredefinedcount=0

					for params in ${paramline[*]}; do
						keyvalues=( ${params//:/ } );
						paramkey=${keyvalues[0]}
						paramvalue=${keyvalues[1]}
#						paramkey=${params//[^a-z0-9]/}
#						paramvalue=${params##*:}
#						debug "paramkey: $paramkey, paramvalue: $paramvalue"

						if [ ! "${params//[^:]/}" = ":" ]; then
							error "$invalidconfigurationtext"
						else
#							debug "*** MEHET ***"
							case $paramkey in
								stationid)
									if [[ -n $stationid ]] && [[ $stationidredefinedcount -eq 0 ]]; then
										warning "$source:$afunctionlinecount: warning: Multiple configuration found for '$paramkey' at Station property!"
									fi

									stationid="$paramvalue"

									if ! szam16 $paramvalue ; then
										error "$source:$afunctionlinecount: error: Invalid value: '$paramvalue' found for key: '$paramkey' at $functionname function! Allowed only integer values between 0 and 65535!"
									fi
								;;

								language)
									if [[ -n $language ]] && [[ $languageredefinedcount -eq 0 ]]; then
										warning "$source:$afunctionlinecount: warning: Multiple configuration found for '$paramkey' at Station property!"
									fi

									language=$(getlangcode "$paramvalue")
								;;

								name)
									if [[ -n $name ]] && [[ $nameredefinedcount -eq 0 ]]; then
										warning "$source:$afunctionlinecount: warning: Multiple configuration found for '$paramkey' at Station property!"
									fi

									name="$paramvalue"
								;;

								*)
									warning "$source:$afunctionlinecount: warning: Unknown configuration key: '$paramkey' found at Station property!"
								;;
							esac
						fi
 					done

					if [[ -z $stationid ]]; then
						error "$source:$afunctionlinecount: error: 'stationid' param is required, but not defined at Station property!"
					fi

					if [[ -z $language ]]; then
						error "$source:$afunctionlinecount: error: 'language' param is requierd, but not defined at Station property or invalid value: '$paramvalue' found for key: 'language' at $functionname function! Allowed iso and hex language ids!"
					fi

					if [[ -z $name ]]; then
						error "$source:$afunctionlinecount: error: 'name' param is required, but not defined at Station property!"
					fi

					if [[ -z $stationnames ]]; then
						stationnames="$stationid;$language;$name"
					else
						stationnames="$stationnames,$stationid;$language;$name"
					fi
				;;

				*)
					warning "$source:$afunctionlinecount: warning: Invalid function or property\n"
				;;
			esac
		fi
	done <<< "$1"

#	debug "setnames: $setnames"
#	debug "stationnames: $stationnames"

	setnamesarr=( ${setnames//,/ } );
	stationnamesarr=( ${stationnames//,/ } );

	if [[ ${#setnamesarr[@]} > 0 ]] || [[ ${#stationnamesarr[@]} > 0 ]]; then
		bufferedlines="\n   // Action4\n"
		imax=${#setnamesarr[@]}
		for ((i=0; i<$imax; i++)); do
			params=( ${setnamesarr[$i]//;/ } );
			asid=${params[0]}

			if hex $asid 2; then
				asid=$(hexraw $asid)
			elif szam8 $asid; then
				asid=$((asid + 50176))
				asid=$(dectohex $asid 4)
			else
				error ""
			fi

			name=$(replaceBackCharacters "${params[2]}")
			bufferedlines+="\t00 * 00\t04 04 ${params[1]} 01 $asid C3 9E \"$name\" 00\n"
		done

		imax=${#stationnamesarr[@]}
		for ((i=0; i<$imax; i++)); do
			params=( ${stationnamesarr[$i]//;/ } );
			asid=${params[0]}

			if hex $asid 2; then
				asid=$(hexraw $asid)
			elif szam8 $asid; then
				asid=$((asid + 50432))
				asid=$(dectohex $asid 4)
			else
				error ""
			fi
			name=$(replaceBackCharacters "${params[2]}")
			bufferedlines+="\t00 * 00\t04 04 ${params[1]} 01 $asid C3 9E \"$name\" 00\n"
		done

		echo -ne "$bufferedlines" >> "$target"
	fi

	echo "$action2autoid"

	if $error ; then returncode=2
	elif $warning ; then returncode=1
	else returncode=0
	fi

	return $returncode
}
