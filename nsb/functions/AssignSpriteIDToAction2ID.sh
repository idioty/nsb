#!/bin/bash

#  AssignSpriteIDToAction2ID.sh
#  nsb
#
#  Created by Kiss Ferenc on 2014.12.23..
#  Copyright (c) 2014 idioty. All rights reserved.

AssignSpriteIDToAction2ID() {
	error=false
	warning=false

	functionname="AssignSpriteIDToAction2ID"
	functionlinecount="$1"
	action2autoid="$3"

	inaddlayoutfunction=false
	inaddlayoutfunctionlines=""
	inaddlayoutbeginlinecount=0
	inaddlayoutlineparameter=""

	action2id=$action2autoid
	action2ida=""

	spriteid=""

	bufferlines=""

	invalidspriteconfigurationtext="$source:$functionlinecount: error: Invalid configuration at $functionname function!"

	functionparameterlines=( ${2//[[:space:]]/} );
	functionparameterlines=( ${functionparameterlines//,/ } );
#	debug "functionparameterlines: ${functionparameterlines[@]}"

	imax=${#functionparameterlines[@]}
	for ((i=0; i<$imax; i++)); do
		functionparameterline="${functionparameterlines[$i]}"
#		debug "functionparameterline: $functionparameterline"
		functionkey=${functionparameterline%:*}
		functionvalue=${functionparameterline##*:}

#		debug "functionkey: $functionkey, functionvalue: $functionvalue"
		case $functionkey in
			action2id)
				if [[ -n $action2ida ]]; then
					warning "$source:$functionlinecount: warning: Multiple configuration found for '$functionkey' at $functionname function!"
				fi

				action2id=$functionvalue
				action2ida="van"
			;;

			spriteid)
				if [[ -n $spriteid ]]; then
					warning "$source:$functionlinecount: warning: Multiple configuration found for '$functionkey' at $functionname function!"
				fi

				spriteid=$functionvalue
			;;

			*)
				warning "$source:$functionlinecount: warning: Unknown configuration ('$functionkey') found at $functionname function!"
			;;
		esac
	done

	if [[ -z $spriteid ]]; then
		spriteid=$action2id
	fi

	if hex $action2id 1; then
		a2id=$(hexraw $action2id)
	elif szam16 $action2id; then
		a2id=$(dectohex $action2id 2)
	else
		error ""
	fi

	if hex $spriteid 2; then
		sid=$(hexraw $spriteid)
	elif szam8 $spriteid; then
		sid=$(dectohex $spriteid 4)
	else
		error ""
	fi

	echo -ne "   00 * 00\t02 04 $a2id 00 01 $sid\n" >> "$target" #kozepen a 00 01 a little/lots cargo elvalaszto, az action2id csak (dec)255 lehet max

	if [[ $action2id -eq $action2autoid ]]; then
		((action2autoid++))
	fi

	echo "$action2autoid"

	if $error ; then returncode=2
	elif $warning ; then returncode=1
	else returncode=0
	fi

	return $returncode
}
