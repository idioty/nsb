#!/bin/bash

#  AddSprite.sh
#  nsb
#
#  Created by Kiss Ferenc on 2014.07.29..
#  Copyright (c) 2014 idioty. All rights reserved.

AddSprite() {
#	error=false
#	warning=false

#curdate=$(date +%s%3N)
	addspritelinecount="$2"
	addspritebeginlinecount="$2"



	inaddlayoutfunction=false
	inaddlayoutfunctionlines=""
	inaddlayoutbeginlinecount=0
	inaddlayoutlineparameter=""


	spriteid=""
	action2id=""
	groundsprite=""
	groundsprite1=""
	groundsprite2=""


	layout1count=0
	layout2count=0


	groundsprite1isinteger=0
	groundsprite2isinteger=0

	invalidspriteconfigurationtext="$source:$addspritebeginlinecount: error: Invalid configuration at @AddSprite function!"

	layout1spritenumberlines=""
	layout1filenamelines=""
	layout1settinglines=""
	layout1boundingboxlines=""
	layout1realspritelines=""
	layout2spritenumberlines=""
	layout2filenamelines=""
	layout2settinglines=""
	layout2boundingboxlines=""
	layout2realspritelines=""

	addspriteparameterlines=( ${3//,/ } );

	imax=${#addspriteparameterlines[@]}
	for ((i=0; i<$imax; i++)); do
		addspritekey=${addspriteparameterlines[$i]%:*}
		addspritevalue=${addspriteparameterlines[$i]##*:}

		if [[ "${addspriteparams//[^:]/}" = ":" ]]; then
			error "$invalidspriteconfigurationtext" 
		else
#			debug "addspritekey: $addspritekey, addspritevalue: $addspritevalue"

			if [[ -z $addspritekey ]]; then
				error "$invalidspriteconfigurationtext"
			else case $addspritekey in
			    spriteid|action2id)
					if [[ -n ${!addspritekey} ]]; then
						warning "$source:$addspritebeginlinecount: warning: Multiple configuration found for 'spriteid' at @AddSprite function!"
					fi

					eval "$addspritekey"=$addspritevalue

					if [[ $addspritevalue != auto ]] && ! hex $addspritevalue 2 && ! szam16 "$addspritevalue" ; then
						error "$source:$addspritebeginlinecount: error: Invalid value: '$addspritevalue' found for key: '$addspritekey' at @AddSprite function! Allowed 'auto' or integer value between 0 and 65535!"
					fi
			    ;;

			    groundsprite|groundsprite1|groundsprite2)
					if [[ -n ${!addspritekey} ]]; then
						warning "$source:$addspritebeginlinecount: warning: Multiple configuration found for 'groundsprite' at @AddSprite function!"
					fi

					if ! hex $addspritevalue 4 && ! szam32 $addspritevalue; then
						error "$source:$addspritebeginlinecount: error: Invalid value: '$addspritevalue' found for key: '$addspritekey' at @AddSprite function! Allowed 4 hex byte or integer value between 0 and 4294967295!"
					fi

					eval "$addspritekey"="$addspritevalue"
			    ;;

			    *)
					warning "$source:$addspritebeginlinecount: warning: Unknown configuration ('$addspritekey') found at @AddSprite function!"
			    ;;
			    esac
			fi
		fi
	done

	while read -r addspriteline
	do
#		debug "addspriteline :$addspriteline"
		((addspritelinecount++))

		if [[ $addspriteline  == @(''|emptyline) ]]; then
			if $inaddlayoutfunction; then
#				debug sprite COMMENT line
				inaddlayoutfunctionlines+="emptyline"$'\n'
			fi
		elif [[ $addspriteline == @EndAddLayout1 || $addspriteline == @EndAddLayout2 ]]; then
			if [[ $inaddlayoutfunction = false ]]; then
				error "$source:$addspritelinecount: error: Open a layout with @AddLayout1 or @AddLayout2 before you close a layout!"
			else
				if [[ ("$addlayoutnumber" -eq 1 && $addspriteline == @EndAddLayout2) || ("$addlayoutnumber" -eq 2 && $addspriteline == @EndAddLayout1) ]]; then
					error "$source:$addspritelinecount: error: Close layout with @EndAddLayout$addlayoutnumber before you close an other layout!"
				fi

#				debug "inaddlayoutbeginlinecount: $inaddlayoutbeginlinecount"
				addlayoutresult=$(AddLayout "$inaddlayoutfunctionlines" "$inaddlayoutbeginlinecount" "$inaddlayoutlineparameter" "$addlayoutnumber")
				aaddlayoutreturncode=$?
				case $aaddlayoutreturncode in
					1) warning=true ;;
					2) error=true ;;
				esac
				IFS=: read -r line0 line1 line2 line3 line4 <<<"$addlayoutresult"

				eval layout${addlayoutnumber}spritenumberlines+="\$line0:"
				eval layout${addlayoutnumber}filenamelines+="\$line1:"
				eval layout${addlayoutnumber}boundingboxlines+="\$line2:"

				if [[ -n $line4 ]];then
					eval layout${addlayoutnumber}settinglines+="\$line3:"
					eval layout${addlayoutnumber}realspritelines+="\$line4:"
				fi

				inaddlayoutfunction=false
				inaddlayoutfunctionlines=""
				inaddlayoutbeginlinecount=0
			fi
		elif $inaddlayoutfunction; then
			if [[ $addspriteline == @AddLayout* ]]; then
				error "$source:$addspritelinecount: error: Close layout with @EndAddLayout$addlayoutnumber before you add a new layout!"
			else
			inaddlayoutfunctionlines+="$addspriteline"$'\n'
			fi
		elif [[ $addspriteline == @AddLayout* ]]; then
			addlayoutnumber=${addspriteline:10:1}
#			debug addlayoutnumber: $addlayoutnumber

#			if ([[ ! $addlayoutnumber =~ [12] ]]) ; then
			case $addlayoutnumber in
			    1) ((layout1count++)) ;;
			    2) ((layout2count++)) ;;
			    *) error "$source:$addspritelinecount: error: Invalid function name! Use '@AddLayout1' or '@AddLayout2'!" ;;
			esac

			inaddlayoutlineparameter=${addspriteline: 10}
			inaddlayoutfunction=true
			inaddlayoutfunctionlines=""
			inaddlayoutbeginlinecount=$addspritelinecount
		else
			warning "$source:$addspritelinecount: warning: Invalid function or property"
		fi
	done <<< "$1"

	if [[ $layout1count -eq 0 ]]; then
		error "$source:$addspritebeginlinecount: error: Invalid configuration at @AddSprite function! @AddLayout1 function is required!"
	elif [[ $layout2count -eq 0 ]]; then
		error "$source:$addspritebeginlinecount: error: Invalid configuration at @AddSprite function! @AddLayout2 function is required!"
	elif [[ $layout1count -ne $layout2count ]]; then
		error "$source:$addspritebeginlinecount: error: Invalid configuration at @AddSprite function! Equal amount required of @AddLayout1 and @AddLayout2!"
	fi

	# ha nincs meghatarozva groundsprite1 vagy groundsprite2, de a groundsprite meg van, akkor bemasolom azt
	if [[ ! -z $groundsprite ]]; then
		if [[ -z $groundsprite1 ]]; then
			groundsprite1=$groundsprite
		fi

		if [[ -z $groundsprite2 ]]; then
			groundsprite2=$groundsprite
		fi
	fi

	if [[ -z $groundsprite1 ]]; then
		groundsprite1="00 00 00 00"
	fi

	if [[ -z $groundsprite2 ]]; then
		groundsprite2="00 00 00 00"
	fi

	# ha van action2id, de nincs spriteid, akkor a spriteid megkapja az auto flag-et
	if [[ -n "$action2id" ]] && [[ -z "$spriteid" ]]; then
		spriteid="auto"
	fi

	# ha van spriteid, de nincs action2id, akkor az action2id megkapja az auto flag-et
	if [[ -n "$spriteid" ]] && [[ -z "$action2id" ]]; then
		action2id="auto"
	fi

	# ez fontos igy, mert ha nem igy van, akkor nem veszi ureskent az erteket, ha nem hataroztuk meg egyiket sem.
	if [[ -n "$spriteid" ]]; then
		spriteids+=("$spriteid")
	fi
	# ez fontos igy, mert ha nem igy van, akkor nem veszi ureskent az erteket, ha nem hataroztuk meg egyiket sem.
	if [[ -n "$action2id" ]]; then
		action2ids+=("$action2id")
	fi

	groundsprites1+=("$groundsprite1")

	spritenumbers1+=("${layout1spritenumberlines%:}")
	filenames1+=("${layout1filenamelines%:}")
	boundingboxes1+=("${layout1boundingboxlines%:}")

	if [[ -z $layout1realspritelines ]]; then
		groundsprites2+=("$groundsprite2");
		spritenumbers2+=("${layout2spritenumberlines%:}")
		filenames2+=("${layout2filenamelines%:}")
		boundingboxes2+=("${layout2boundingboxlines%:}")
	else
		settings1+=("${layout1settinglines%:}")
		realsprites1+=("${layout1realspritelines%:}")
		groundsprites2+=("$groundsprite2");
		spritenumbers2+=("${layout2spritenumberlines%:}")
		filenames2+=("${layout2filenamelines%:}")
		boundingboxes2+=("${layout2boundingboxlines%:}")
		settings2+=("${layout2settinglines%:}")
		realsprites2+=("${layout2realspritelines%:}")
	fi

#	if $error ; then returncode=2
#	elif $warning ; then returncode=1
#	else returncode=0
#	fi
#
#	return $returncode
}
