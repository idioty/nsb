#!/bin/bash

#  AddStation.sh
#  nsb
#
#  Created by Kiss Ferenc on 2014.07.29..
#  Copyright (c) 2014 idioty. All rights reserved.

AddStation() {
	error=false
	warning=false

	functionlinecount="$2"
	astationlinecount="$2"
	properties=0
	spriteparamters=""
	action2autoid=0

	inaddspritefunction=false
	inaddspritefunctionlines=""
	inaddspritebeginlinecount=0

	# required peroperties at function parameters
	stationid=""
	stationclass=""
	auto2id=""
	aspidtemp=""
	autospriteid=0

	continueaction2id=""

	# optional peroperties in function
	pylon=""
	pylonline=""
	pylonfirstline=""
	pylonredefinedcount=0
	pylonlastline=""

	wire=""
	wireline=""
	wirefirstline=""
	wireredefinedcount=0
	wirelastline=""

	train=""
	trainline=""
	trainfirstline=""
	trainredefinedcount=0
	trainlastline=""

	littlelotsthreshold=""
	littlelotsthresholdline=""
	littlelotsthresholdfirstline=""
	littlelotsthresholdredefinedcount=0

	callbackflags=""
	callbackflagsline=""
	callbackflagsfirstline=""
	callbackflagsredefinedcount=0

	platforms=""
	platformsline=""
	platformsfirstline=""
	platformsredefinedcount=0

	length=""
	lengthline=""
	lengthfirstline=""
	lengthredefinedcount=0

	definecustomlayout=""
	definecustomlayoutline=""
	definecustomlayoutfirstline=""
	definecustomlayoutredefinedcount=0

	copyspritelayout=""
	copyspritelayoutline=""
	copyspritelayoutfirstline=""
	copyspritelayoutredefinedcount=0

	copycustomlayout=""
	copycustomlayoutline=""
	copycustomlayoutfirstline=""
	copycustomlayoutredefinedcount=0

	animationinformation=""
	animationinformationline=""
	animationinformationfirstline=""
	animationinformationredefinedcount=0

	animationspeed=""
	animationspeedline=""
	animationspeedfirstline=""
	animationspeedredefinedcount=0

	animationtriggers=""
	animationtriggersline=""
	animationtriggersfirstline=""
	animationtriggersredefinedcount=0

	roadrouting=""
	roadroutingline=""
	roadroutingfirstline=""
	roadroutingredefinedcount=0

	cargotypesforrandomtriggers=""
	cargotypesforrandomtriggersline=""
	cargotypesforrandomtriggersfirstline=""
	cargotypesforrandomtriggersredefinedcount=0

	generalflags=""
	generalflagsline=""
	generalflagsfirstline=""
	generalflagsredefinedcount=0


	declare -a sprite1beginlines
	declare -a sprite2beginlines
	declare -a spriteids
	declare -a action2ids
	declare -a groundsprites1
	declare -a groundsprites2
	declare -a spritenumbers1
	declare -a spritenumbers2
	declare -a filenames1
	declare -a filenames2
	declare -a boundingboxes1
	declare -a boundingboxes2
	declare -a settings1
	declare -a settings2
	declare -a realsprites1
	declare -a realsprites2

	invalidstationconfigurationtext="$source:$functionlinecount: error: Invalid configuration at AddStation function\n"
	ipcwsie=0 # ezzel fogom megjelolni, hogy van sprite hozzaadva, ezert novelni kell a properties szamat, es ha mar noveltem, akkor ez 1 lesz es megegyszser nem kell novelni...

	addstationparameterlines=( ${3//[[:space:]]/} );
	addstationparameterlines=( ${addstationparameterlines//,/ } );

	imax=${#addstationparameterlines[@]}
	for ((i=0; i<$imax; i++)); do
		addstationparameterline="${addstationparameterlines[$i]}"

		addstationkey=${addstationparameterline%:*}
		addstationvalue=${addstationparameterline##*:}

		if [[ ! ${addstationparameterline//[^:]/} = ":" ]]; then
			case $addstationkey in
				continueaction2id)
					if [[ ! -z $continueaction2id ]]; then
						warning "$source:$functionlinecount: warning: Multiple configuration found for '$addstationkey' at AddStation function\n"
					fi

					action2autoid="$4"
				;;

				*)
					error "$invalidstationconfigurationtext"
				;;
			esac
		else
			

#			debug "addstationkey: $addstationkey, addstationvalue: $addstationvalue"

#			if [[ $addstationkey == id ]]; then
			case $addstationkey in
			    id)
					if [[ -n $stationid ]]; then
						warning "$source:$functionlinecount: warning: Multiple configuration found for 'id' at AddStation function!"
					fi

					stationid=$addstationvalue
				;;

			    class)
					if [[ -n $stationclass ]]; then
						warning "$source:$functionlinecount: warning: Multiple configuration found for 'class' at AddStation function!"
					fi

					stationclass=$addstationvalue

					if ! szam32 $stationclass && [[ ${#stationclass} -ne 4 ]] ; then
						error "$source:$functionlinecount: error: Invalid value: '$stationclass' found for key: 'stationclass' at AddStation function! Allowed 4 characters or integer value between 0 and 4294967295!"
					fi

					((properties++))
				;;

			    auto2id)
					if [[ -n $auto2id ]]; then
						warning "$source:$functionlinecount: warning: Multiple configuration found for '$addstationkey' at AddStation function!"
					fi

					auto2id=$addstationvalue

					if szam16 $auto2id ; then
						action2autoid=$auto2id
					else
						error "$source:$functionlinecount: error: Invalid value: '$auto2id' found for key: 'auto2id' at AddStation function! Allowed only integer value between 0 and 65535!"
					fi
				;;

			    autospriteid)
					if [[ -n $aspidtemp ]]; then
						warning "$source:$functionlinecount: warning: Multiple configuration found for '$addstationkey' at AddStation function!"
					fi

					aspidtemp="vandefinialva..." # ez azert van itt, mert lehet 0 is az autospriteid, es igy konnyebb ellenorizni a redefined countot
					autospriteid=$addstationvalue

					if szam16 $autospriteid ; then
						action2autoid=$autospriteid
					else
						error "$source:$functionlinecount: error: Invalid value: '$autospriteid' found for key: 'autospriteid' at AddStation function! Allowed only integer value between 0 and 65535!"
					fi
				;;

			    *)
					warning "$source:$functionlinecount: warning: Unknown configuration ('$addstationkey') found at AddStation function!"
				;;
			esac
		fi
	done

	while read -r addstationline
	do
		# delete comments, trim
		addstationline=${addstationline/'#'*/}
		addstationline=${addstationline/\/\/*/}
		addstationline=${addstationline//[[:space:]]/}

# az utolso trim emlekere
#		addstationline=$(trim $addstationline)
# RIP

		((astationlinecount++))
#		debug "line[$astationlinecount]: $addstationline"
		if [[ $addstationline == @('//'*|'#'*|'') ]]; then
			if $inaddspritefunction; then
				inaddspritefunctionlines+="emptyline"$'\n'
			fi
		elif [[ $addstationline == @EndAddSprite ]]; then
#			debug "endaddsprite: $astationlinecount"
#			debug inaddspritebeginlinecount="$inaddspritebeginlinecount"
#			debug spriteparameters="$spriteparamters"
#			addspriteresult=$(AddSprite "$inaddspritefunctionlines" "$inaddspritebeginlinecount" "$spriteparamters")

			AddSprite "$inaddspritefunctionlines" "$inaddspritebeginlinecount" "$spriteparamters" >/dev/null
			# nem subshell, igy eleg felulirni az error vagy warning valtozot az AddSprite/ban
#			aaddstationreturncode=$?
#			if [[ $addstationreturncode -lt $aaddstationreturncode ]]; then
#				addstationreturncode=$aaddstationreturncode
#			fi

			# ahany boundingbox van, annyi beginline-t hozzaadunk, hogy tudjuk melyik boundingboxhoz melyik spritebegin sor tartozik
			for bbi in ${boundingboxes1[@]}; do
				sprite1beginlines+=("$inaddspritebeginlinecount")
			done

			for bbi in ${#boundingboxes2[*]}; do
				sprite2beginlines+=("$inaddspritebeginlinecount")
			done

#			IFS=$SAVEIFS

			inaddspritefunction=false
			inaddspritefunctionlines=""
			inaddspritebeginlinecount=0
		elif $inaddspritefunction; then
			if [[ $addstationline == @AddSprite* ]]; then
				error "$source:$astationlinecount: error: Close sprite with @EndAddSprite before you add a new sprite!"
			else
				inaddspritefunctionlines+="$addstationline"$'\n'
			fi
		else case $addstationline in
		    @AddSprite:*)
				if [[ $ipcwsie -eq 0 ]]; then
					((properties++))
					ipcwsie=1
				fi

#				spriteparamters="${addstationline:11}"
				spriteparamters="${addstationline#*:}"
#				debug "beginaddsprite: $spriteparamters"
				inaddspritefunction=true
				inaddspritefunctionlines=""
				inaddspritebeginlinecount=$astationlinecount
		    ;;

		    Pylon:*)
				if [[ -z $pylon ]]; then
					pylonfirstline=$astationlinecount
				else
					pylonline+="$astationlinecount,"
					((pylonredefinedcount++))
					warning "$source:$pylonfirstline: warning: Pylon property is redefined at line $astationlinecount!"
				fi

				pylonlastline=$astationlinecount
				pylon=${addstationline#*:}
				((properties++))
		    ;;

		    Wire:*)
				if [[ -z $wire ]]; then
					wirefirstline=$astationlinecount
				else
					wireline="$astationlinecount,"
					((wireredefinedcount++))
					warning "$source:$wirefirstline: warning: Wire property is redefined at line $astationlinecount!"
				fi

				wirelastline=$astationlinecount
				wire=${addstationline#*:}
				((properties++))
		    ;;

		    Train:*)
				if [[ -z $train ]]; then
					trainfirstline=$astationlinecount
				else
					trainline+="$astationlinecount,"
					((trainredefinedcount++))
					warning "$source:$trainfirstline: warning: Train property is redefined at line $astationlinecount!"
				fi

				trainlastline=$astationlinecount
				train=${addstationline#*:}
				((properties++))
		    ;;

		    Little/LotsThreshold:*)
				if [[ -z $littlelotsthreshold ]]; then
					littlelotsthresholdfirstline=$astationlinecount
				else
					littlelotsthresholdline+="$astationlinecount,"
					((littlelotsthresholdredefinedcount++))
					warning "$source:$littlelotsthresholdfirstline: warning: CallbackFlags property is redefined at line $astationlinecount!"
				fi

				littlelotsthreshold=${addstationline#*:}
				((properties++))
		    ;;

		    CallbackFlags:*)
				if [[ -z $callbackflags ]]; then
					callbackflagsfirstline=$astationlinecount
				else
					callbackflagsline+="$astationlinecount,"
					((callbackflagsredefinedcount++))
					warning "$source:$callbackflagsfirstline: warning: CallbackFlags property is redefined at line $astationlinecount!"
				fi

				callbackflags=${addstationline#*:}
				((properties++))
		    ;;

		    Platforms:*)
				if [[ -z $platforms ]]; then
					platformsfirstline=$astationlinecount
				else
					platformsline+="$astationlinecount,"
					((platformsredefinedcount++))
					warning "$source:$platformsfirstline: warning: Platforms property is redefined at line $astationlinecount!"
				fi

				platforms=${addstationline#*:}
				((properties++))
		    ;;

		    Length:*)
				if [[ -z $length ]]; then
					lengthfirstline=$astationlinecount
				else
					lengthline+="$astationlinecount,"
					((lengthredefinedcount++))
					warning "$source:$lengthfirstline: warning: Length property is redefined at line $astationlinecount!"
				fi

				length=${addstationline#*:}
				((properties++))
		    ;;

		    DefineCustomLayout:*)
				if [[ -z $definecustomlayout ]]; then
					definecustomlayoutfirstline=$astationlinecount
				else
					definecustomlayoutline+="$astationlinecount,"
					((definecustomlayoutredefinedcount++))
					warning "$source:$definecustomlayoutfirstline: warning: Length property is redefined at line $astationlinecount!"
				fi

				definecustomlayout=${addstationline#*:}
				((properties++))
		    ;;

		    CopySpriteLayout:*)
				if [[ -z $copyspritelayout ]]; then
					copyspritelayoutfirstline=$astationlinecount
				else
					copyspritelayoutline+="$astationlinecount,"
					((copyspritelayoutredefinedcount++))
					warning "$source:$copyspritelayoutfirstline: warning: Length property is redefined at line $astationlinecount!"
				fi

				copyspritelayout=${addstationline#*:}
				((properties++))
		    ;;

		    CopyCustomLayout:*)
				if [[ -z $copycustomlayout ]]; then
					copycustomlayoutfirstline=$astationlinecount
				else
					copycustomlayoutline+="$astationlinecount,"
					((copycustomlayoutredefinedcount++))
					warning "$source:$copycustomlayoutfirstline: warning: Length property is redefined at line $astationlinecount!"
				fi

				copycustomlayout=${addstationline#*:}
				((properties++))
		    ;;

		    AnimationInformation:*)
				if [[ -z $animationinformation ]]; then
					animationinformationfirstline=$astationlinecount
				else
					animationinformationline+="$astationlinecount,"
					((animationinformationredefinedcount++))
					warning "$source:$animationinformationfirstline: warning: Length property is redefined at line $astationlinecount!"
				fi

				animationinformation=${addstationline#*:}
				((properties++))
		    ;;

		    AnimationSpeed:*)
				if [[ -z $animationspeed ]]; then
					animationspeedfirstline=$astationlinecount
				else
					animationspeedline+="$astationlinecount,"
					((animationspeedredefinedcount++))
					warning "$source:$animationspeedfirstline: warning: Length property is redefined at line $astationlinecount!"
				fi

				animationspeed=${addstationline#*:}
				((properties++))
		    ;;

		    AnimationTriggers:*)
				if [[ -z $animationtriggers ]]; then
					animationtriggersfirstline=$astationlinecount
				else
					animationtriggersline+="$astationlinecount,"
					((animationtriggersredefinedcount++))
					warning "$source:$animationtriggersfirstline: warning: Length property is redefined at line $astationlinecount!"
				fi

				animationtriggers=${addstationline#*:}
				((properties++))
		    ;;

		    RoadRouting:*)
				if [[ -z $roadrouting ]]; then
					roadroutingfirstline=$astationlinecount
				else
					roadroutingline+="$astationlinecount,"
					((roadroutingredefinedcount++))
					warning "$source:$roadroutingfirstline: warning: Length property is redefined at line $astationlinecount!"
				fi

				roadrouting=${addstationline#*:}
				((properties++))
		    ;;

		    CargoTypesforRandomTriggers:*)
				if [[ -z $cargotypesforrandomtriggers ]]; then
					cargotypesforrandomtriggersfirstline=$astationlinecount
				else
					cargotypesforrandomtriggersline+="$astationlinecount,"
					((cargotypesforrandomtriggersredefinedcount++))
					warning "$source:$cargotypesforrandomtriggersfirstline: warning: Length property is redefined at line $astationlinecount!"
				fi

				cargotypesforrandomtriggers=${addstationline#*:}
				((properties++))
		    ;;

		    GeneralFlags:*)
				if [[ -z $generalflags ]]; then
					generalflagsfirstline=$astationlinecount
				else
					generalflagsline+="$astationlinecount,"
					((generalflagsredefinedcount++))
					warning "$source:$generalflagsfirstline: warning: Length property is redefined at line $astationlinecount!"
				fi

				generalflags=${addstationline#*:}
				((properties++))
		    ;;

		    *)
				warning "$source:$astationlinecount: warning: Invalid function or property\n"
		    ;;
		esac
		fi
	done <<< "$1"



	if [[ -z $stationid ]]; then
		error "$source:$functionlinecount: error: 'id' property is required, but not defined in this function!"
	fi

	if [[ -z $stationclass ]]; then
		error "$source:$functionlinecount: error: 'class' property is required, but not defined in this function!"
	fi

	if [[ $pylonredefinedcount -gt 0 ]]; then
		lines=( ${pylonline//,/} );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			warning "$source:${lines[$i]}: warning: Pylon property is already defined at line $pylonfirstline!"
		done
	fi

	if [[ $wireredefinedcount -gt 0 ]]; then
		lines=( ${wireline//,/} );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			warning "$source:${lines[$i]}: warning: Wire property is already defined at line $wirefirstline!"
		done
	fi

	if [[ $trainredefinedcount -gt 0 ]]; then
		lines=( ${trainline//,/} );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			warning "$source:${lines[$i]}: warning: Train property is already defined at line $trainfirstline!"
		done
	fi

	if [[ $littlelotsthresholdredefinedcount -gt 0 ]]; then
		lines=( ${littlelotsthresholdline//,/} );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			warning "$source:${lines[$i]}: warning: CallbackFlags property is already defined at line $littlelotsthresholdfirstline!"
		done
	fi

	if [[ $callbackflagsredefinedcount -gt 0 ]]; then
		lines=( ${callbackflagsline//,/} );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			warning "$source:${lines[$i]}: warning: CallbackFlags property is already defined at line $callbackflagsfirstline!"
		done
	fi

	if [[ $platformsredefinedcount -gt 0 ]]; then
		lines=( ${platformsline//,/} );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			warning "$source:${lines[$i]}: warning: Platforms property is already defined at line $platformsfirstline!"
		done
	fi

	if [[ $lengthredefinedcount -gt 0 ]]; then
		lines=( ${lengthline//,/} );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			warning "$source:${lines[$i]}: warning: Length property is already defined at line $lengthfirstline!"
		done
	fi

	if [[ $definecustomlayoutredefinedcount -gt 0 ]]; then
		lines=( ${definecustomlayoutline//,/} );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			warning "$source:${lines[$i]}: warning: Length property is already defined at line $definecustomlayoutfirstline!"
		done
	fi

	if [[ $copyspritelayoutredefinedcount -gt 0 ]]; then
		lines=( ${copyspritelayoutline//,/} );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			warning "$source:${lines[$i]}: warning: Length property is already defined at line $copyspritelayoutfirstline!"
		done
	fi

	if [[ $copycustomlayoutredefinedcount -gt 0 ]]; then
		lines=( ${copycustomlayoutline//,/} );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			warning "$source:${lines[$i]}: warning: Length property is already defined at line $copycustomlayoutfirstline!"
		done
	fi

	if [[ $animationinformationredefinedcount -gt 0 ]]; then
		lines=( ${animationinformationline//,/} );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			warning "$source:${lines[$i]}: warning: Length property is already defined at line $animationinformationfirstline!"
		done
	fi

	if [[ $animationspeedredefinedcount -gt 0 ]]; then
		lines=( ${animationspeedline//,/} );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			warning "$source:${lines[$i]}: warning: Length property is already defined at line $animationspeedfirstline!"
		done
	fi

	if [[ $animationtriggersredefinedcount -gt 0 ]]; then
		lines=( ${animationtriggersline//,/} );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			warning "$source:${lines[$i]}: warning: Length property is already defined at line $animationtriggersfirstline!"
		done
	fi

	if [[ $roadroutingredefinedcount -gt 0 ]]; then
		lines=( ${roadroutingline//,/} );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			warning "$source:${lines[$i]}: warning: Length property is already defined at line $roadroutingfirstline!"
		done
	fi

	if [[ $cargotypesforrandomtriggersredefinedcount -gt 0 ]]; then
		lines=( ${cargotypesforrandomtriggersline//,/} );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			warning "$source:${lines[$i]}: warning: Length property is already defined at line $cargotypesforrandomtriggersfirstline!"
		done
	fi

	if [[ $generalflagsredefinedcount -gt 0 ]]; then
		lines=( ${generalflagsline//,/} );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			warning "$source:${lines[$i]}: warning: Length property is already defined at line $generalflagsfirstline!"
		done
	fi

	# konvertalas hexava...
	properties=$(dectohex $properties 2)

#	stationid=$(dectohex $stationid 2)
	if hex "$stationid" 1; then
		stationid=$(hexraw "$stationid")
	elif szam8 $stationid; then
		stationid=$(dectohex $stationid 2)
	else
		error ""
	fi

	bufferedlines+="\n   // Action0\\n"
	bufferedlines+="   00 * 00\t00 04\t\t\t\t// Action0, Feature4\\n"

# station count fixen 01 most
	bufferedlines+="\t\t\t$properties 01 $stationid\t\t\t// Properties, Stations, First Station ID\\n"
	bufferedlines+="\t\t\t08 \"$stationclass\"\t\t\t// Station Class\\n"


#
#	Copy Sprite Layout
#
#	http://newgrf-specs.tt-wiki.net/wiki/Action0/Stations#Copy_sprite_layout_.280A.29
#
	if [[ -n $copyspritelayout ]]; then
        if hex "$copyspritelayout" 1; then
			copyspritelayout=$(hexraw "$copyspritelayout")
		else
#			egyelore csak hex lehet
			error ""
		fi
		bufferedlines+="\t\t\t0A $copyspritelayout\t\t\t\t// Copy Sprite Layout\\n"
	fi

#
#	Callback Flags
#
#	http://newgrf-specs.tt-wiki.net/wiki/Action0/Stations#Callback_flags_.280B.29
#	Szinten bitmas alapu, egyelore nem legyeg...
#
	if [[ -n $callbackflags ]]; then
#		callbackflags=$(dectohex $callbackflags 2)
        if hex "$callbackflags" 1; then
			callbackflags=$(hexraw "$callbackflags")
		else
#			egyelore csak hex lehet
			error ""
		fi
		bufferedlines+="\t\t\t0B $callbackflags\t\t\t\t// Callback Flags\\n"
	fi

#
#	Platforms
#
#	http://newgrf-specs.tt-wiki.net/wiki/Action0/Stations#Selection_of_numbers_of_platforms_and_length_.280C.2C_0D.29
#
	if [[ -n $platforms ]]; then
        if hex "$platforms" 1; then
			platforms=$(hexraw "$platforms")
		else
#			egyelore csak hex lehet
			error ""
		fi
		bufferedlines+="\t\t\t0C $platforms\t\t\t\t// Platforms\\n"
	fi


#
#	Length
#
#	http://newgrf-specs.tt-wiki.net/wiki/Action0/Stations#Selection_of_numbers_of_platforms_and_length_.280C.2C_0D.29
#
	if [[ -n $length ]]; then
        if hex "$length" 1; then
			length=$(hexraw "$length")
		else
#			egyelore csak hex lehet
			error ""
		fi
		bufferedlines+="\t\t\t0D $length\t\t\t\t// Length\\n"
	fi


#
#	Define Custom Layout
#
#	http://newgrf-specs.tt-wiki.net/wiki/Action0/Stations#Define_custom_layout_.280E.29
#
	if [[ -n $definecustomlayout ]]; then
		# ez majd lehet kicsit bonyolultabb lesz, mert 3 reszbol all, az elsp bajt a hossza a vaganyoknak masodik a peronok szama, es utana pedig ennek a kettonek a szorzata bajt
        if hex "$definecustomlayout"; then
			definecustomlayout=$(hexraw "$definecustomlayout")
		else
#			egyelore csak hex lehet
			error ""
		fi
		bufferedlines+="\t\t\t0E $definecustomlayout\t\t\t\t// Define Custom Layout\\n"
	fi

#
#	Copy Custom Layout
#
#	http://newgrf-specs.tt-wiki.net/wiki/Action0/Stations#Little.2Flots_threshold_.2810.29
#
	if [[ -n $copycustomlayout ]]; then
		if hex "$copycustomlayout" 1; then
			copycustomlayout="$(hexraw $copycustomlayout)"
		else
#			egyelore csak hex lehet
			error ""
		fi
		bufferedlines+="\t\t\t0F $copycustomlayout\t\t\t\t// Copy Custom Layout\\n"
	fi


#
#	Little/Lots Threshold
#
#	http://newgrf-specs.tt-wiki.net/wiki/Action0/Stations#Callback_flags_.280B.29
#	Szinten bitmas alapu, egyelore nem legyeg...
#
	if [[ -n $littlelotsthreshold ]]; then
#		littlelotsthreshold=$(dectohex $littlelotsthreshold 4 true spaces)
		if hex "$littlelotsthreshold" 2; then
			littlelotsthreshold=$(hexraw "$littlelotsthreshold")
		elif szam16 $littlelotsthreshold; then
			littlelotsthreshold=$(dectohex $littlelotsthreshold 4)
		else
			error ""
		fi
		bufferedlines+="\t\t\t10 $littlelotsthreshold\t\t\t\t// Little/Lots Threshold\\n"
	fi

#
#	Pylon - villanyoszlop
#
#	http://newgrf-specs.tt-wiki.net/wiki/Action0/Stations#Pylon_placement_.2811.29_and_wire_placement_.2814.29
#	Tiletype szerint lehet megadni bitmask szeruen, hogy melyiken lehet es melyiken nem.
#	Tiletypes:	http://newgrf-specs.tt-wiki.net/wiki/Action0/Stations#Define_custom_layout_.280E.29
#	Ha jol ertem: 0-3 teto nelkulinel alapbol van, mig a 4-7 tetonel nincs.
#	Tehat 00 mindnel kikapcoslja, mig az FF az osszesnel (vagy jobban mondva a 7F-nel is, ha az FF hibat dobna) bekapcsolja.
#
	if [[ -n $pylon ]]; then
		if [[ $pylon == enabled ]]; then
			pylon="FF"
		elif [[ $pylon == disabled ]]; then
			pylon="00"
		elif hex "$pylon" 1; then
			pylon=$(hexraw "$pylon")
		else
			error "$source:$pylonlastline: error: Invalid Pylon property value: '$pylon'!"
		fi

		bufferedlines+="\t\t\t11 $pylon\t\t\t\t// Pylon disabled\\n"
	fi

#
#	Cargo Types for Random Triggers
#
#	http://newgrf-specs.tt-wiki.net/wiki/Action0/Stations#Cargo_types_for_random_triggers_.2812.29
#
	if [[ -n $cargotypesforrandomtriggers ]]; then
        if hex "$cargotypesforrandomtriggers"; then
			cargotypesforrandomtriggers=$(hexraw "$cargotypesforrandomtriggers")
		else
#			egyelore csak hex lehet
			error ""
		fi
		bufferedlines+="\t\t\t12 $cargotypesforrandomtriggers\t\t\t// Cargo Types for Random Triggers\\n"
	fi

#
#	General Flags
#
#	http://newgrf-specs.tt-wiki.net/wiki/Action0/Stations#General_Flags_.2813.29
#
	if [[ -n $generalflags ]]; then
        if hex "$generalflags" 1; then
			generalflags=$(hexraw "$generalflags")
		else
#			egyelore csak hex lehet
			error ""
		fi
		bufferedlines+="\t\t\t13 $generalflags\t\t\t// General Flags\\n"
	fi

#
#	Wire - villanyveyetek
#
#	http://newgrf-specs.tt-wiki.net/wiki/Action0/Stations#Pylon_placement_.2811.29_and_wire_placement_.2814.29
#	Tiletype szerint lehet megadni bitmask szeruen, hogy melyiken lehet es melyiken nem.
#	Tiletypes:	http://newgrf-specs.tt-wiki.net/wiki/Action0/Stations#Define_custom_layout_.280E.29
#	Ha jol ertem: 0-3 teto nelkulinel alapbol van, mig a 4-7 tetonel nincs. Itt forditva vani, mint a Pylonnal, ha a bit be van allitva, akkor kapcsolja ki.
#	Tehat 00 mindnel bekapcoslja, mig az FF az osszesnel (vagy jobban mondva a 7F-nel is, ha az FF hibat dobna) kikapcsolja.
#
	if [[ -n $wire ]]; then
		if [[ $wire == enabled ]]; then
			wire="00"
		elif [[ $wire == disabled ]]; then
			wire="FF"
		elif hex "$wire" 1; then
			wire=$(hexraw "$wire")
		else
			error "$source:$wirelastline: error: Invalid Wire property value: '$wire'!"
		fi
		bufferedlines+="\t\t\t14 $wire\t\t\t\t// Wire disabled\\n"
	fi

#
#	Train - bemehetneke a vonatok
#
#	http://newgrf-specs.tt-wiki.net/wiki/Action0/Stations#Can_train_enter_tile_.2815.29
#	Tiletype szerint lehet megadni bitmask szeruen, hogy melyiken lehet es melyiken nem.
#	Tiletypes:	http://newgrf-specs.tt-wiki.net/wiki/Action0/Stations#Define_custom_layout_.280E.29
#	Ha jol ertem: 0-3 teto nelkulinel alapbol van, mig a 4-7 tetonel nincs. Itt forditva vani, mint a Pylonnal, ha a bit be van allitva, akkor kapcsolja ki.
#	Tehat 00 mindnel bekapcoslja, mig az FF az osszesnel (vagy jobban mondva a 7F-nel is, ha az FF hibat dobna) kikapcsolja, azaz nem mehet be a vonat
#
	if [[ -n $train ]]; then
		if [[ $train == enabled ]]; then
			train="00"
		elif [[ $train == disabled ]]; then
			train="FF"
		elif hex "$train" 1; then
			train=$(hexraw "$train")
		else
			error "$source:$trainlastline: error: Invalid Train property value: '$train'!"
		fi
		bufferedlines+="\t\t\t15 $train\t\t\t\t// Train disabled\\n"
	fi
#
#	Animation Information
#
#	http://newgrf-specs.tt-wiki.net/wiki/Action0/Stations#Animation_information_.2816.29
#
	if [[ -n $animationinformation ]]; then
        if hex "$animationinformation" 2; then
			animationinformation=$(hexraw "$animationinformation")
		else
#			egyelore csak hex lehet
			error ""
		fi
		bufferedlines+="\t\t\t16 $animationinformation\t\t\t// Animation Information\\n"
	fi

#
#	Animation Speed
#
#	http://newgrf-specs.tt-wiki.net/wiki/Action0/Stations#Animation_speed_.2817.29
#
	if [[ -n $animationspeed ]]; then
        if hex "$animationspeed" 1; then
			animationspeed=$(hexraw "$animationspeed")
		else
#			egyelore csak hex lehet
			error ""
		fi
		bufferedlines+="\t\t\t17 $animationspeed\t\t\t\t// Animation Speed\\n"
	fi

#
#	Animation Triggers
#
#	http://newgrf-specs.tt-wiki.net/wiki/Action0/Stations#Animation_triggers_.2818.29
#
	if [[ -n $animationtriggers ]]; then
        if hex "$animationtriggers" 2; then
			animationtriggers=$(hexraw "$animationtriggers")
		else
#			egyelore csak hex lehet
			error ""
		fi
		bufferedlines+="\t\t\t18 $animationtriggers\t\t\t// Animation Triggers\\n"
	fi

#
#	Road Routing
#
#	http://newgrf-specs.tt-wiki.net/wiki/Action0/Stations#Road_routing_.2819_-_reserved_for_future_use.29
#
	if [[ -n $roadrouting ]]; then
        if hex "$roadrouting"; then
			roadrouting=$(hexraw "$roadrouting")
		else
#			egyelore csak hex lehet
			error ""
		fi
		bufferedlines+="\t\t\t19 $roadrouting\t\t\t// Road Routing\\n"
	fi

#
#	Sprite layout, Action1, Action2
#
#	http://newgrf-specs.tt-wiki.net/wiki/Action0/Stations#Sprite_layout_.2809.29
#	http://newgrf-specs.tt-wiki.net/wiki/Action1
#
	if [[ ${#spritenumbers1[@]} -gt 0 ]]; then

		# elso dolgunk, hogy leellenorizzuk, hogy ugyanannyi layoutbol all-e az osszes sprite

		SAVEIFS=$IFS
		IFS=":";

		fbdsz=${#boundingboxes1[0]};
		slimax=${#boundingboxes1[@]}
		for ((sli=1; sli<$slimax; sli++)); do # direkt 1-tol kezdodik a ciklus, hogy magaval ne ellenorizze
			if [[ ${#boundingboxes1[$sli]} -ne $fbdsz ]]; then
				error "sli: $sli, $source:${sprite1beginlines[$sli]}: error: All Sprite Layout count must equal!!!"
			fi
		done

		slimax=${#boundingboxes2[@]}
		for ((sli=0; sli<$slimax; sli++)); do
			if [[ ${#boundingboxes2[$sli]} -ne $fbdsz ]]; then
				error "$source:${sprite2beginlines[$sli]}: error: All Sprite Layout count must equal!!!"
			fi
		done

		spcount=$(dectohex $[${#spritenumbers1[@]} * 2] 2)
		bufferedlines+="\n\t\t\t09 $spcount\t\t\t\t// Sprite Layout\\n"

		# Layoutok kiiratasa

		# az biztos, hogy ahany groundsprite van, annyi sprite-ot kell kirajzolni
		slimax=${#groundsprites1[@]}
		for ((sli=0; sli<$slimax; sli++)); do
			# groundsprites1
			grsp="${groundsprites1[$sli]}"
			if hex $grsp 4; then
				grsp=$(hexraw $grsp)
			elif szam32 $grsp; then
				grsp=$(dectohex $grsp 8 true spaces)
			fi
			bufferedlines+="\n\t\t\t$grsp\t\t\t// Layout1 groundsprite\\n"

			# sprite boundingbox1

			bboxes1=( ${boundingboxes1[$sli]} );
			snumbers1=( ${spritenumbers1[$sli]} );
			bsimax=${#bboxes1[@]}
			for ((bsi=0; bsi<$bsimax; bsi++)); do
				bufferedlines+="\t\t\t${bboxes1[$bsi]}\t\t${snumbers1[$bsi]}\\n"
			done

			# end layout
			bufferedlines+="\t\t\t80\t\t\t\t\t// end layout1 sprite\n\\n"


			# groundsprites2
			grsp="${groundsprites2[$sli]}"
			if hex $grsp 4; then
				grsp=$(hexraw $grsp)
			elif szam32 $grsp; then
				grsp=$(dectohex $grsp 8 true spaces)
			fi
			bufferedlines+="\t\t\t$grsp\t\t\t// Layout2 groundsprite\\n"

			# sprite boundingbox2

			bboxes2=( ${boundingboxes2[$sli]} );
			snumbers2=( ${spritenumbers2[$sli]} );

			bsimax=${#bboxes2[@]}
			for ((bsi=0; bsi<$bsimax; bsi++)); do
				bufferedlines+="\t\t\t${bboxes2[$bsi]}\t\t${snumbers2[$bsi]}\\n"
			done

			# end layout
			bufferedlines+="\t\t\t80\t\t\t\t\t// end layout1 sprite\n\\n"
		done

		IFS=$SAVEIFS

		# Action 1 megjelenitese
		if [[ ${#realsprites1[@]} -gt 0 ]]; then
#			counter=0
#			lrsc=0 # eltaroljuk, hogy a for cikluson belul a realsprite hany darab van, es ha nem egyezik az elozovel, akkor ujabb Action1-et hozunk letre az uj beallitasokkal
#			maxcount=0

			SAVEIFS=$IFS

#			debug "realsprites1 ${realsprites1[@]}"
			countermax=${#realsprites1[@]}
			saslimax=${#realsprites1[@]}
			for ((sasli=0; sasli<$saslimax;)); do
				IFS=":";
				counter=$sasli
				maxcount=0		# ez pedig a layoutcount lett (persze meg meg kell szorozni kettovel...)
				lrsc=0			# ez lett a sprite count
				kesz=0

#				debug "1"

#				debug "counter: $counter, countermax = $countermax"
				while [[ $kesz -eq 0 ]] && [[ $counter -lt $countermax ]]; do
					rsps=( ${realsprites1[$counter]} );
#					debug "rsps: $rsps"

					if [[ $maxcount -eq 0 ]]; then
						maxcount=${#rsps[@]}
						((lrsc++))
					else
						if [[ $maxcount -eq ${#rsps[@]} ]]; then
							((lrsc++))
						else
							kesz=1
						fi
					fi
					((counter++))
#					debug "counter: $counter, kesz: $kesz"
				done
#				debug "2, lrsc: $lrsc"

				maxcount=$(dectohex $[maxcount * 2] 2)
				alrsc=$(dectohex $lrsc 2)

				bufferedlines+="\n   // Action1\\n"
				bufferedlines+="   00 * 00\t01 04 $alrsc $maxcount\\n"

				slimax=$[sasli + lrsc]
				for ((sli=sasli; sli<$slimax; sli++)); do
					IFS=":";
					rsps1=( ${realsprites1[$sli]} );
					rsps2=( ${realsprites2[$sli]} );
					st1=( ${settings1[$sli]} );
					st2=( ${settings2[$sli]} );
					fn1=( ${filenames1[$sli]} );
					fn2=( ${filenames2[$sli]} );

					IFS=",";
					rslimax=${#rsps1[@]}
					for ((rsli=0; rsli<$rslimax; rsli++)); do
						asettings=( ${st1[$rsli]} );
						bufferedlines+="   00 ${fn1[$rsli]} ${asettings[0]} ${rsps1[$rsli]} ${asettings[1]} ${asettings[2]}\\n"
					done

					rslimax=${#rsps2[@]}
					for ((rsli=0; rsli<$rslimax; rsli++)); do
						asettings=( ${st2[$rsli]} );
						bufferedlines+="   00 ${fn2[$rsli]} ${asettings[0]} ${rsps2[$rsli]} ${asettings[1]} ${asettings[2]}\\n"
					done
				done

				sasli=$((sasli+lrsc))
#				debug "3, sasli: $sasli"
			done

			IFS=$SAVEIFS
		fi

		if ! [[ $action2ids == "" ]]; then
			spriteautoid=$autospriteid

			bufferedlines+="\n   // Action2\\n"

			saslimax=${#action2ids[@]}
			for ((sasli=0; sasli<$saslimax; sasli++)); do
				if [[ "${action2ids[$sasli]}" == auto ]]; then
					a2id=$action2autoid
					((action2autoid++))
					a2id=$(dectohex $a2id 2 true spaces)
				else
					a2id="${action2ids[$sasli]}"
					if hex $a2id 1; then
						a2id=$(hexraw $a2id)
					elif szam8 $a2id; then
						a2id=$(dectohex $a2id 2 true spaces)
					else
						error ""
					fi
				fi

				if [[ "${spriteids[$sasli]}" == auto ]]; then
					sid=$spriteautoid
					((spriteautoid++))
					sid=$(dectohex $sid 4 true spaces)
				else
					sid="${spriteids[$sasli]}"
					if hex $sid 2; then
						sid=$(hexraw $sid)
					elif szam16 $sid; then
						sid=$(dectohex $sid 4 true spaces)
					else
						error ""
					fi
				fi

#				a2id=$(dectohex $a2id 2 true spaces)
#				sid=$(dectohex $sid 4 true spaces)

				# kozepen a 00 01 a little/lots cargo elvalaszto, az action2id csak (dec)255 lehet max
				bufferedlines+="   00 * 00\t02 04 $a2id 00 01 $sid\\n"
			done
		fi
	fi

	echo -ne "$bufferedlines" >> "$target"

	# TODO: itt vissza kell terni az action2 max ID-val, hogy tudjam folytatni mas funkciokban az ID-t
	echo "$action2autoid"

	if $error ; then returncode=2
	elif $warning ; then returncode=1
	else returncode=0
	fi

	return $returncode
}
