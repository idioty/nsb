#!/bin/bash

#  AddSprite.sh
#  nsb
#
#  Created by Kiss Ferenc on 2014.07.29..
#  Copyright (c) 2014 idioty. All rights reserved.

AddVariationalAction2() {
	error=false
	warning=false

	functionname="AddVariationalAction2"
	functionlinecount="$2"
	functionbeginlinecount="$2"

	inaddlayoutfunction=false
	inaddlayoutfunctionlines=""
	inaddlayoutbeginlinecount=0
	inaddlayoutlineparameter=""

	type=""
	variable=""
	varadjust=""
	masking=""

	declare -a addranges
	rangecount=0
	bytelength=0
	bytes=0

	bufferlines=""
	rangeslines=""

	invalidspriteconfigurationtext="$source:$functionbeginlinecount: error: Invalid configuration at $functionname function!"

	functionparameterlines=( ${3//[[:space:]]/} );
	functionparameterlines=( ${functionparameterlines//,/ } );
#	debug "functionparameterlines: $functionparameterlines"

	imax=${#functionparameterlines[@]}
	for ((i=0; i<$imax; i++)); do
		functionparameterline="${functionparameterlines[$i]}"
#		debug "functionparameterline: $functionparameterline"
		functionkey=${functionparameterline%:*}
		functionvalue=${functionparameterline##*:}

#		debug "functionkey: $functionkey, functionvalue: $functionvalue"

		if [[ "${functionparams//[^:]/}" = ":" ]]; then
			error "$invalidspriteconfigurationtext"
		else
			if [[ -z $functionkey ]]; then
				error "$invalidspriteconfigurationtext"
			else case $functionkey in
#			    if [[ $functionkey == type ]]; then
			    type)
					if [[ -n $type ]]; then
						warning "$source:$functionbeginlinecount: warning: Multiple configuration found for 'type' at $functionname function!"
					fi

					if hex $functionvalue 1; then
						aval=$(hexremove $functionvalue)
						type=$(hexraw $functionvalue)

						case $aval in
							81|82) bytelength=2; bytes=1 ;;
							85|86) bytelength=4; bytes=2 ;;
							89|8A) bytelength=8; bytes=4 ;;
							*)
								error "$source:$functionbeginlinecount: error: Invalid value: '$aval' found for key: 'type' at $functionname function! Allowed only 81, 82, 85, 86, 89, 8A values!"
							;;
						esac
					else
						error "$source:$functionbeginlinecount: error: The type paramter value must be 1 byte!"
					fi
			    ;;

			    variable)
					if [[ -n $variable ]]; then
						warning "$source:$functionbeginlinecount: warning: Multiple configuration found for 'variable' at $functionname function!"
					fi

					if hex $functionvalue 1; then
						variable=$(hexraw $functionvalue)
					else
						error "$source:$functionbeginlinecount: error: The variable paramter value must be 1 bute!"
					fi
			    ;;

			    varadjust)
					if [[ -n $varadjust ]]; then
						warning "$source:$functionbeginlinecount: warning: Multiple configuration found for 'varadjust' at $functionname function!"
					fi

					if hex $functionvalue 1; then
						varadjust=$(hexraw $functionvalue)
					else
						error "$source:$functionbeginlinecount: error: The varadjust paramter value must be 1 byte!"
					fi
				;;

			    masking)
					if [[ -n $masking ]]; then
						warning "$source:$functionbeginlinecount: warning: Multiple configuration found for 'masking' at $functionname function!"
					fi

					# direkt nem tesztelem itt, mert a helyes tulajdonsaghoz tudnom kellene a meretet a type-bol, ami meg lehet kesobb is...
					masking=$functionvalue
				;;

			    *)
					warning "$source:$functionbeginlinecount: warning: Unknown configuration ('$functionkey') found at $functionname function!"
			    ;;
			    esac
			fi
		fi
	done

	while read -r functionline
	do
#		debug "functionline: $functionline"
		((functionlinecount++))

		propertykey=${functionline}
		propertykey=${propertykey/'#'*/}
		propertykey=${propertykey/\/\/*/}
		propertykey=${propertykey//[[:space:]]/}
		propertyvalue=${propertykey#*:}

#		debug "propertykey: $propertykey, propertyvalue: $propertyvalue"

		if [[ $propertykey != @('//'*|'#'*|'') ]]; then
			case $propertykey in
				AddRange:*)
					paramline=( ${propertyvalue//,/ } );
					((rangecount++))
					parammin=""
					parammax=""
					paramaction2id=""

					for params in ${paramline[*]}; do
						keyvalues=( ${params//:/ } );
						paramkey=${keyvalues[0]}
						paramvalue=${keyvalues[1]}
#						debug "paramkey: $paramkey, paramvalue: $paramvalue"

						if [ ! "${params//[^:]/}" = ":" ]; then
							error "$invalidconfigurationtext"
						else
#							debug "*** MEHET ***: $paramkey"
							case $paramkey in
								min)
#									debug "min :$paramvalue"
									parammin="$paramvalue"
								;;

								max)
#									debug "max :$paramvalue"
									parammax="$paramvalue"
								;;

								action2id)
#									debug "action2id :$paramvalue"
									paramaction2id="$paramvalue"
								;;

								*)
									warning "$source:$afunctionlinecount: warning: Unknown configuration key: '$paramkey' found at AddRange property!"
								;;
							esac
						fi
					done
					# TODO: chekkolni kell az osszes tulajdonsagot, mert mind kotelezo, azaz a min, max es action2id is kotelezo

#					aaction2id=$(dectohex $paramaction2id 4 true spaces)
					if hex $paramaction2id 2; then
						paramaction2id=$(hexraw $paramaction2id)
					elif szam16 $paramaction2id; then
						paramaction2id=$(dectohex $paramaction2id 4)
					else
						error ""
					fi
					aaction2id=$paramaction2id

#					aamin=$(dectohex $parammin $bytelength true spaces)
					if hex $parammin $bytes; then
						parammin=$(hexraw $parammin)
					elif szam8 $parammin; then
						parammin=$(dectohex $parammin $bytelength)
					else
						error ""
					fi
					aamin=$parammin

#					aamax=$(dectohex $parammax $bytelength true spaces)
					if hex $parammax $bytes; then
						parammax=$(hexraw $parammax)
					elif szam8 $parammax; then
						parammax=$(dectohex $parammax $bytelength)
					else
						error ""
					fi
					aamax=$parammax

#					debug "rangeslines + = $aaction2id | $aamin | $aamax"
					rangeslines+="\t\t\t$aaction2id\t$aamin\t$aamax\t\t//action2id, min range, max range\n"
				;;
			esac
		fi
	done <<< "$1"

	if hex $masking $bytes; then
		aval=$(hexremove $masking)
		masking=$(hexraw $masking)
		if [[ ${#aval} -ne $bytelength ]]; then
			error "$source:$functionbeginlinecount: error: The masking paramter value must be $bytes byte(s) length with this type: $type!"
		fi
	else
		error "$source:$functionbeginlinecount: error: The masking paramter value must be $bytes byte(s) length hex data with this type: $type!"
	fi

	if [[ $rangecount -eq 0 ]]; then
		error "$source:$functionbeginlinecount: error: You must last one AddRange parameter!"
	fi

	rc=$(dectohex $rangecount 2 false)

	tk=""
	case $bytelength in
		2) tk="\t\t\t" ;;
		4) tk="\t\t" ;;
		8) tk="\t" ;;
	esac

	echo -n "$type $variable $varadjust\t\t//type, variable, varadjust\n\t\t\t$masking\t$tk//masking\n\t\t\t$rc\t\t\t\t//range count\n$rangeslines"

	if $error ; then returncode=2
	elif $warning ; then returncode=1
	else returncode=0
	fi

	return $returncode
}
