
AddLittleLotsSets() {
	error=false
	warning=false

	functionname="AddLittleLotsSets"
	functionlinecount="$2"
	afunctionlinecount="$2"
	action2autoid="$4"

	infunction=false
	functionreturnlines=""

#	parameterlines=( ${3//,/ } );

	invalidconfigurationtext="$source:$functionlinecount: error: Invalid configuration at $functionname function!"

	action2id=$action2autoid
	action2idredefinedcount=0
	action2ida=""

	adddefault=""
	adddefaultline=""
	adddefaultfirstline=""
	adddefaultredefinedcount=0

	littlecount=0
	addlittlesets=""
	addlittlesetsline=""
	addlittlesetsfirstline=""
	addlittlesetsredefinedcount=0

	lotscount=0
	addlotssets=""
	addlotssetsline=""
	addlotssetsfirstline=""
	addlotssetsredefinedcount=0

	needdefault=true

	parameterlines=( ${3//[[:space:]]/} );
	parameterlines=( ${parameterlines//,/ } );

	imax=${#parameterlines[@]}
	for ((i=0; i<$imax; i++)); do
		parameterline="${parameterlines[$i]}"
#		debug "functionparameterline: $functionparameterline"
		paramkey=${parameterline%:*}
		paramvalue=${parameterline##*:}
#
#		parameterline="${parameterlines[$i]}"
#		params=( ${parameterline//:/ } );
#
#		paramkey=${parameterline%:*}
#		paramvalue=${parameterline##*:}

#		if [[ ! ${parameterline//[^:]/} = ":" ]]; then
		if [[ ${parameterline//[^:]/} = ":" ]]; then
#			debug "van parameter"
#			case $paramkey in
#				continueaction2id)
#					if [[ ! -z $continueaction2id ]] && [[ $continueaction2idredefinedcount -eq 0 ]]; then
#						warning "$source:$functionlinecount: warning: Multiple configuration found for '$paramkey' at $functionname function\n"
#					fi
#
#					action2autoid="$4"
#				;;
#
#				*)
#					error "$invalidstationconfigurationtext"
#				;;
#			esac
#		else
#			debug "key: $paramkey, value: $paramvalue"
			case $paramkey in
				id)
					if [[ ! -z $action2ida ]]; then
						warning "$source:$functionlinecount: warning: Multiple configuration found for $paramkey at $functionname function!"
					fi

					action2id=$paramvalue
					action2ida="vam"

					if ! [[ $action2id == auto ]]; then
						if ! szam16 $action2id ; then
							error "$source:$inbeginlinecount: error: Invalid value: '$action2id' found for key: 'action2id' at AddAction2 function! Allowed 'auto' or integer value between 0 and 65535!"
						fi
					fi
				;;

				littlesets:*)
					if [[ ! -z $addlittlesets ]]; then
						warning "$source:$functionlinecount: warning: Multiple configuration found for $paramkey at $functionname function!"
					fi

					addlittlesets=$propertyvalue
				;;

				lotssets:*)
					if [[ ! -z $addlotssets ]]; then
						warning "$source:$functionlinecount: warning: Multiple configuration found for $paramkey at $functionname function!"
					fi

					addlotssets=$propertyvalue
				;;

				*)
					warning "$source:$functionlinecount: warning: Unknown configuration ('$paramkey') found at $functionname function!"
				;;
			esac
		fi
	done


	# ide jon a megvalositas
	aaction2id=$(dectohex $action2id 2 false)
	buff="\n\t//Action2\n   00 * 00\t02 04 $aaction2id\n"

	if [[ -n $addlittlesets ]] && [[ -n $addlotssets ]]; then
#		debug "addlittlesets: $addlittlesets"
#		debug "addlotssets: $addlotssets"
		aliclines=( ${addlittlesets//,/ } );
		aloclines=( ${addlotssets//,/ } );
#		debug "aliclines: ${aliclines[@]}"
#		debug "aloclines: ${aloclines[@]}"
		alic=$(dectohex ${#aliclines[@]}  2)
		aloc=$(dectohex ${#aloclines[@]}  2)

		buff+="\t\t\t$alic $aloc\t\t\t//littlesets count, lotssets count\n"

		buff+="\t\t\t"
		imax=${#aliclines[@]}
		for ((i=0; i<$imax; i++)); do
			szam="${aliclines[$i]}"
			if hex $szam 2; then
				szam=$(hexraw $szam)
			elif szam16 $szam; then
				szam=$(dectohex $szam 4 true spaces)
			else
				error "$source:$addlittlesetsfirstline: error: Invalid value: '$szam' found for key: 'LittleSets' at $functionname function! Allowed only integer values between 0 and 65535!"
			fi
			buff+="$szam"
			if [[ $i -lt $imax-1 ]]; then
				buff+=" "
			fi
		done
		buff+="\t\t\t//LittleSets\n"

		buff+="\t\t\t"
		imax=${#aloclines[@]}
		for ((i=0; i<$imax; i++)); do
			szam="${aloclines[$i]}"
			if hex $szam 2; then
				szam=$(hexraw $szam)
			elif szam16 $szam; then
				szam=$(dectohex $szam 4 true spaces)
			else
				error "$source:$addlotssetsfirstline: error: Invalid value: '$szam' found for key: 'LotsSets' at $functionname function! Allowed only integer values between 0 and 65535!"
			fi
			buff+="$szam"
			if [[ $i -lt $imax-1 ]]; then
				buff+=" "
			fi
		done
		buff+="\t\t\t//LotsSets\n"
	else
		if $needdefault; then
			if [[ -z $adddefault ]]; then
				error "$source:$functionlinecount: error: 'Default' property is required, but not defined in this function!"
			elif [[ $adddefaultredefinedcount -gt 0 ]]; then
				lines=( ${adddefaultline//,/} );

				imax=${#lines[@]}
				for ((i=0; i<$imax; i++)); do
					warning "$source:${lines[$i]}: warning: 'Default' property is already defined at line $adddefaultfirstline!"
				done
			fi
		fi

		buff+="\t\t\t$functionreturnlines"
		if $needdefault; then
			buff+="\t\t\t$adddefault\t\t\t//default action2id"
		fi
	fi

	echo -e "$buff" >> "$target"

	if [[ $action2id -eq $action2autoid ]]; then
		((action2autoid++))
	fi

	echo "$action2autoid"

	if $error ; then returncode=2
	elif $warning ; then returncode=1
	else returncode=0
	fi

	return $returncode
}