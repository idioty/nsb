#!/bin/bash

#  AddLayout.sh
#  nsb
#
#  Created by Kiss Ferenc on 2014.07.29..
#  Copyright (c) 2014 idioty. All rights reserved.

AddLayout() {
	error=false
	warning=false
#curdate=$(date +%s%3N)
	addlayoutlinecount="$2"
	addlayoutbeginlinecount="$2"
#	addlayoutcommentcount=0

	addlayoutnumber="$4"

#	spritenumber=""
#	spritenumberredefinedcount=0
#	filename=""
#	filenameredefinedcount=0


#	settings=""
#	settingsline=""
#	settingsfirstline=""
#	settingsredefinedcount=0
#	settingslastline=""

#	boundingbox=""
#	boundingboxline=""
#	boundingboxfirstline=""
#	boundingboxredefinedcount=0
#	boundingboxlastline=""

#	realsprite=""
#	realspriteline=""
#	realspritefirstline=""
#	realspriteredefinedcount=0
#	realspritelastline=""

#	colormode=0
#	colormoderedefinedcount=0


	invalidlayoutconfigurationtext="$source:$addlayoutbeginlinecount: error: Invalid configuration at @AddLayout function!"

	addlayoutparameter="${3}"

	if ([[ ! $addlayoutnumber == [12] ]]) ; then
		error "$source:$addlayoutbeginlinecount: error: Invalid function name! Use '@AddLayout1' or '@AddLayout2'!"
	fi

	additionalspritenumber=1069

	addlayoutparameter=${addlayoutparameter#*:}
	addlayoutparameterlines=( ${addlayoutparameter//,/ } );

	for addlayoutparameterline in ${addlayoutparameter//,/ };do
#		debug "addlayoutparameterline: $addlayoutparameterline"
		addlayoutkey=${addlayoutparameterline%:*}
		if [ ! "${addlayoutparameterline//[^:]/}" = ":" ]; then
#			command=$addlayoutkey
			# transparent commands, ha kellene masikat ellenorizni, akkor az kulon alatta levo ifben kellene megcsinalni
			if [[ $addlayoutkey == transparent || $addlayoutkey == companycolor ]]; then
				if [[ $colormode -ne 0 ]]; then
					warning "$source:$addlayoutlinecount: warning: Color mode property is redefined (with: $addlayoutkey)!"
				fi

				if [[ $addlayoutkey == transparent ]]; then
					colormode=2
					additionalspritenumber=52577325
				else
					colormode=1
					additionalspritenumber=33837
				fi

				((colormoderedefinedcount++))
			else
				error "$source:$addlayoutbeginlinecount: warning: Unknown configuration ('$addlayoutkey') found at @AddLayout$addlayoutnumber function!"
			fi
		else
			addlayoutvalue=${addlayoutparameterline##*:}
#			debug "addlayoutkey: $addlayoutkey, addlayoutvalue: $addlayoutvalue"
			case $addlayoutkey in
			    spritenumber)
#debug spritenumber
					if [[ -n $spritenumber ]]; then
# && [[ $spritenumberredefinedcount -eq 0 ]]
						warning "$source:$addlayoutbeginlinecount: warning: Multiple configuration found for 'spritenumber' at @AddLayout$addlayoutnumber function!"
					fi

					spritenumber=$addlayoutvalue
			    ;;

			    filename)
#debug filename
					if [[ -n $filename ]];then
# && [[ $filenameredefinedcount -eq 0 ]]; 
						warning "$source:$addlayoutbeginlinecount: warning: Multiple configuration found for 'filename' at @AddLayout$addlayoutnumber function!"
					fi

					filename=$addlayoutvalue
			    ;;

			    *)
					warning "$source:$addlayoutbeginlinecount: warning: Unknown configuration ('$addlayoutkey') found at @AddLayout$addlayoutnumber function!"
			    ;;
			esac
		fi
	done


	while read -r addlayoutline
	do
		((addlayoutlinecount++))

#		debug "addlayoutlinecount: $addlayoutlinecount"
#		debug "addlayoutline: $addlayoutline"

		if [[  $addlayoutline == @(''|emptyline) ]]; then
			# csak azert tettem ezt ide, hogy legyen itt valami
			((addlayoutcommentcount++))
		else case $addlayoutline in
		    Settings:*)
				if [[ -z $settings ]]; then
					settingsfirstline=$addlayoutlinecount
				else
					settingsline+="$addlayoutlinecount,"
					warning "$source:$settingsfirstline: warning: Settings property is redefined at line $addlayoutlinecount!"
				fi

				((settingsredefinedcount++))
				addlayoutsettingsparams=${addlayoutline#*:}
				settings=$(AddLayoutSettings "$addlayoutsettingsparams" "$addlayoutlinecount")
				aaddlayoutreturncode=$?
				case $aaddlayoutreturncode in
					1) warning=true ;;
					2) error=true ;;
				esac
		    ;;

		    Boundingbox:*)
				if [[ -z $boundingbox ]]; then
					boundingboxfirstline=$addlayoutlinecount
				else
					boundingboxline+="$addlayoutlinecount,"
					((boundingboxredefinedcount++))
					warning "$source:$boundingboxfirstline: warning: Boundingbox property is redefined at line $addlayoutlinecount!"
				fi

				addlayoutboundingboxparams=${addlayoutline#*:}
				boundingbox=$(AddLayoutBoundingbox "$addlayoutboundingboxparams" "$addlayoutlinecount")
				aaddlayoutreturncode=$?
				case $aaddlayoutreturncode in
					1) warning=true ;;
					2) error=true ;;
				esac
		    ;;

		    RealSprite:*)
				if [[ -z $realsprite ]]; then
					realspritefirstline=$addlayoutlinecount
				else
					realspriteline+="$addlayoutlinecount,"
					((realspriteredefinedcount++))
					warning "$source:$realspritefirstline: warning: RealSprite property is redefined at line $addlayoutlinecount!"
				fi

				addlayoutrealspriteparams=${addlayoutline#*:}
				realsprite=$(AddLayoutRealSprite "$addlayoutrealspriteparams" "$addlayoutlinecount")
				aaddlayoutreturncode=$?
				case $aaddlayoutreturncode in
					1) warning=true ;;
					2) error=true ;;
				esac
		    ;;

		    *)
				warning "$source:$addlayoutlinecount: warning: Invalid property\n"
		    ;;
		    esac
		fi
	done <<< "$1"

	# tehat akkor ki van hagyva a Settings tulajdonsag (mivel lehet opcionalis), igy alapertelmezettet kell berakni
	if [[ -z $settings ]]; then
		settings="8bpp,normal,chunked"
	fi

	# spritenumber is opcionalis parameter
	if [[ -z $spritenumber ]]; then
		if [[ $addlayoutnumber -eq 1 ]]; then
			spritenumber=0
		else
			spritenumber=1
		fi
	fi

	if hex $spritenumber 4 ; then
		spritenumber=$(hexraw $spritenumber)
	elif szam16 $spritenumber  ; then
		spritenumber=$((spritenumber + additionalspritenumber))
		spritenumber=$(dectohex $spritenumber 8 true spaces)
	else
		error "$source:$addlayoutbeginlinecount: error: Invalid value: '$spritenumber' found for key: 'spritenumber' at @AddLayout$addlayoutnumber function! Allowed value between 0 and 65535!"
	fi


	if [[ -z $filename ]]; then
		error "$source:$addlayoutbeginlinecount: error: 'filename' property is required, but not defined in this function!"
	fi

	if [[ -z $boundingbox ]]; then
		error "$source:$addlayoutbeginlinecount: error: Boundingbox property is required, but not defined in this function!"
	elif [[ $boundingboxredefinedcount -gt 0 ]]; then
		lines=( ${boundingboxline//,/ } );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			aline="${lines[$i]}"
			warning "$source:$aline: warning: Boundingbox property is already defined at line $boundingboxfirstline\n"
		done
	fi

	if [[ $realspriteredefinedcount -gt 0 ]]; then
		lines=( ${realspriteline//,/ } );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			aline="${lines[$i]}"
			warning "$source:$aline: warning: RealSprite property is already defined at line $realspritefirstline\n"
		done
	fi

	if [[ -z $realsprite ]]; then
		echo "$spritenumber:$filename:$boundingbox"
	else
		echo "$spritenumber:$filename:$boundingbox:$settings:$realsprite"
	fi

	if $error ; then returncode=2
	elif $warning ; then returncode=1
	else returncode=0
	fi

	return $returncode
}
