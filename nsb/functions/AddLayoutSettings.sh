#!/bin/bash

#  AddLayoutSettings.sh
#  nsb
#
#  Created by Kiss Ferenc on 2014.08.02..
#  Copyright (c) 2014 idioty. All rights reserved.

#Settings: type: 8bpp, zoomlevel: normal, flags: chunked

AddLayoutSettings() {
	error=false
	warning=false

	addlayoutsettingsparameter="$1"

	addlayoutsettingslinecount="$2"
#	addlayoutsettingsreturncode=0
	addlayoutsettingsbeginlinecount="$2"
	addlayoutsettingscommentcount=0


	type=""
	zoomlevel=""
	flags=""

	invalidaddlayoutsettingsconfigurationtext="$source:$addlayoutsettingsbeginlinecount: error: Invalid configuration at Settings property!"

	addlayoutsettingsparameterlines=( ${addlayoutsettingsparameter//,/ } );

	imax=${#addlayoutsettingsparameterlines[@]}
	for ((i=0; i<$imax; i++)); do
		addlayoutsettingsparameterline="${addlayoutsettingsparameterlines[$i]}"
#		debug "paramline: $addlayoutsettingsparameterline"
                addlayoutsettingskey=${addlayoutsettingsparameterline%:*}
                addlayoutsettingsvalue=${addlayoutsettingsparameterline##*:}

		if [[ ! "${addlayoutsettingsparameterline//[^:]/}" = ":" ]]; then
			error "$invalidaddlayoutsettingsconfigurationtext"
		else
#			debug "addlayoutsettingskey: $addlayoutsettingskey, addlayoutsettingsvalue: $addlayoutsettingsvalue"
			case $addlayoutsettingskey in
				type)
					if [[ ! -z $type ]]; then
						warning "$source:$addlayoutsettingsbeginlinecount: warning: Multiple configuration key: 'type' found at Settings property!"
					fi

					type=$addlayoutsettingsvalue

					if [[ ! $type == 8bpp ]] && [[ ! $type == 32bpp ]] && [[ ! $type == mask ]]; then
						error "$source:$addlayoutsettingsbeginlinecount: error: Invalid value: '$type' found for key: 'type' at Settings property! Allowed values: '8bpp', '32bpp', 'mask'!"
					fi
				;;

				zoomlevel)
					if [[ ! -z $zoomlevel ]]; then
						warning "$source:$addlayoutsettingsbeginlinecount: warning: Multiple configuration key: 'zoomlevel' found at Settings property!"
					fi

					zoomlevel=$addlayoutsettingsvalue

					if [[ ! $zoomlevel == normal ]] && [[ ! $zoomlevel == zo8 ]] && [[ ! $zoomlevel == zo4 ]] && [[ ! $zoomlevel == zo2 ]] && [[ ! $zoomlevel == zi2 ]] && [[ ! $zoomlevel == zi4 ]]; then
						error "$source:$addlayoutsettingsbeginlinecount: error: Invalid value: '$zoomlevel' found for key: 'zoomlevel' at Settings property! Allowed values: 'normal', 'zo8', 'zo4', 'zo2', 'zi2', 'zi4'!"
					fi
				;;

				flags)
					if [[ ! -z $flags ]]; then
						warning "$source:$addlayoutsettingsbeginlinecount: warning: Multiple configuration key: 'flags' found at Settings property!"
					fi

					flags=$addlayoutsettingsvalue
					if [[ ! $flags == nocrop ]] && [[ ! $flags == chunked ]]; then
						error "$source:$addlayoutsettingsbeginlinecount: error: Invalid value: '$flags' found for key: 'flags' at Settings property! Allowed values: 'nocrop', 'chunked'!"
					fi
				;;

				*)
					warning "$source:$addlayoutsettingsbeginlinecount: warning: Unknown configuration key: '$addlayoutsettingskey' found at Settings property!"
				;;
			esac
		fi
	done


	#optional parameters, we must define if undefined
	if [[ -z $type ]]; then
		type="8bpp"
	elif [[ $type == mask ]]; then # elvileg ha mask az ertek, akkor egybol utana kell a 32bpp szoveg
		type="32bpp mask"
	fi

	if [[ -z $zoomlevel ]]; then
		zoomlevel="normal"
	fi

	if [[ -z $flags ]]; then
		flags="chunked"
	fi

#	warning "$source:$addlayoutsettingsbeginlinecount: warning: type: $type!"
	echo "$type,$zoomlevel,$flags"

	if $error ; then returncode=2
	elif $warning ; then returncode=1
	else returncode=0
	fi

	return $returncode
}