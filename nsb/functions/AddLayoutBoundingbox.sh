#!/bin/bash

#  AddLayoutBoundingbox.sh
#  nsb
#
#  Created by Kiss Ferenc on 2014.08.02..
#  Copyright (c) 2014 idioty. All rights reserved.

AddLayoutBoundingbox() {
#	Boundingbox: offsetx: 0, offsety: 0, offsetz: 0, sizex: 0, sizey: 0, sizez: 0

	addlayoutboundingboxparameter="$1"

	addlayoutboundingboxlinecount="$2"
	addlayoutboundingboxbeginlinecount="$2"
	addlayoutboundingboxcommentcount=0

	error=false
	warning=false

	sharedboundingbox=0

	invalidaddlayoutboundingboxconfigurationtext="$source:$addlayoutboundingboxbeginlinecount: error: Invalid configuration at Boundingbox property!"

	addlayoutboundingboxparameterlines=( ${addlayoutboundingboxparameter//,/ } );

	for addlayoutboundingboxparameterline in ${addlayoutboundingboxparameter//,/ };do

#		debug "paramline: $addlayoutboundingboxparameterline"

		key=${addlayoutboundingboxparameterline%:*}
		addlayoutboundingboxvalue=${addlayoutboundingboxparameterline##*:}

		if [ ! "${addlayoutboundingboxparameterline//[^:]/}" = ":" ]; then
			if [[ $key == shared ]]; then
				if [[ $sharedboundingbox -eq 1 ]]; then
					warning "$source:$addlayoutboundingboxbeginlinecount: warning: Multiple configuration key: '$key' found at Boundingbox property!"
					((key++))
				fi
				sharedboundingbox=1
			else
				error "$invalidaddlayoutboundingboxconfigurationtext"
			fi
		else

#			debug "key: $key, addlayoutboundingboxvalue: $addlayoutboundingboxvalue"
			if [[ $key =~ (offsetx|offsety|offsetz|sizex|sizey|sizez) ]]; then
				if [[ -n ${!key} ]]; then
					warning "$source:$addlayoutboundingboxbeginlinecount: warning: Multiple configuration key: '$key' found at Boundingbox property!"
				fi
				eval "$key"=$addlayoutboundingboxvalue
				eval rc=boxrange_${key%?}
				if ! ${!rc} $addlayoutboundingboxvalue; then
					eval boxrangeerror=boxrangeerror_${key%?}
					error "$source:$addlayoutboundingboxbeginlinecount: error: Invalid value: '$value' found for key: '$key' at Boundingbox property! Allowed only integer value between ${!boxrangeerror}!"
				fi
			else
				warning "$source:$addlayoutboundingboxbeginlinecount: warning: Unknown configuration key: '$key' found at Boundingbox property!"
			fi
		fi
	done

	if [[ $sharedboundingbox -eq 0 ]]; then
		for key in offsetx offsety offsetz sizex sizey sizez; do
			if [[ -z ${!key} ]]; then
			    error "$source:$addlayoutboundingboxbeginlinecount: error: '$key' param is required, but not defined at Boundingbox property!"
			fi
		done
	fi

# 00 00 00	00 00 00
	if [[ $sharedboundingbox -eq 1 ]]; then
		echo "00 00 80\t00 00 00"
	else
#[speed hack]
		printf "%02X %02X %02X\\\t%02X %02X %02X\n" $offsetx $offsety $offsetz $sizex $sizey $sizez 2>/dev/null
	fi

	if $error ; then returncode=2
	elif $warning ; then returncode=1
	else returncode=0
	fi

	return $returncode
}
