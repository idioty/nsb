#!/bin/bash

#  AddStation.sh
#  nsb
#
#  Created by Kiss Ferenc on 2014.07.29..
#  Copyright (c) 2014 idioty. All rights reserved.

AssignGrapghicsSets() {
	error=false
	warning=false

	functionname="AssignGrapghicsSets"
	functionlinecount="$2"
	afunctionlinecount="$2"
	action2autoid="$4"

	invalidconfigurationtext="$source:$functionlinecount: error: Invalid configuration at $functionname function!"


	stationids=""
	stationidsline=""
	stationidsfirstline=""
	stationidsredefinedcount=0
	stationidscount=0

	cargoidscount=0

	addmenu=""
	addmenuline=""
	addmenufirstline=""
	addmenuredefinedcount=0

	addcenter=""
	addcenterline=""
	addcenterfirstline=""
	addcenterredefinedcount=0

	addcargocargoids=""
	addcargoaction2ids=""

	adddefault=""
	adddefaultline=""
	adddefaultfirstline=""
	adddefaultredefinedcount=0

	while read -r propertykey
	do
		# delete comments, trim
		propertykey=${propertykey/'#'*/}
		propertykey=${propertykey/\/\/*/}
		propertykey=${propertykey//[[:space:]]/}
		propertyvalue=${propertykey#*:}

		((afunctionlinecount++))
#		debug "line: $addstationline"
		if [[ $propertykey != @('//'*|'#'*|'') ]]; then
			case $propertykey in
				StationIDs:*)
					if [[ -z $stationids ]]; then
						stationidsfirstline=$afunctionlinecount
					else
						stationidsline+="$afunctionlinecount,"
						((stationidsredefinedcount++))
						warning "$source:$stationidsfirstline: warning: 'StationIDs' property is redefined at line $afunctionlinecount!"
					fi

					allids=( ${propertyvalue//,/ } );
					imax=${#allids[@]}
					for ((i=0; i<$imax; i++)); do
						((stationidscount++))
						aid=${allids[$i]}

						if hex $aid 1; then
							aid=$(hexraw $aid)
						elif szam8 $aid; then
							if [[ -n $stationids ]]; then
								stationids+=" "
							fi
							aid=$(dectohex $aid 2 false)
						else
							error "$source:$afunctionlinecount: error: Invalid value: '$propertyvalue' found for key: 'StationIDs' at $functionname function! Allowed only integer value between 0 and 255!"
						fi

						stationids+=$aid
					done
				;;

				AddMenu:*)
					if [[ -z $addmenu ]]; then
						addmenufirstline=$afunctionlinecount
					else
						addmenuline+="$afunctionlinecount,"
						((addmenuredefinedcount++))
						warning "$source:$addmenufirstline: warning: 'AddMenu' property is redefined at line $afunctionlinecount!"
					fi

					if hex $propertyvalue 2; then
						propertyvalue=$(hexraw $propertyvalue)
					elif szam16 $propertyvalue; then
						propertyvalue=$(dectohex $propertyvalue 4 true spaces)
					else
						error "$source:$afunctionlinecount: error: Invalid value: '$propertyvalue' found for key: 'AddMenu' at $functionname function! Allowed only integer value between 0 and 65535!"
					fi

					addmenu=$propertyvalue
					((cargoidscount++))

				;;

				AddCenter:*)
					if [[ -z $addcenter ]]; then
						addcenterfirstline=$afunctionlinecount
					else
						addcenterline+="$afunctionlinecount,"
						((addcenterredefinedcount++))
						warning "$source:$addcenterfirstline: warning: 'AddCenter' property is redefined at line $afunctionlinecount!"
					fi

					if hex $propertyvalue 2; then
						propertyvalue=$(hexraw $propertyvalue)
					elif szam16 $propertyvalue; then
						propertyvalue=$(dectohex $propertyvalue 4 true spaces)
					else
						error "$source:$afunctionlinecount: error: Invalid value: '$propertyvalue' found for key: 'AddCenter' at $functionname function! Allowed only integer value between 0 and 65535!"
					fi

					addcenter=$propertyvalue
					((cargoidscount++))
				;;

				AddCargo:*)
#					debug "propertyvalue: $propertyvalue"
					paramline=( ${propertyvalue//,/ } );

					acargoid=""
					acargoidredefinedcount=0
					aaction2id=""
					aaction2idredefinedcount=0

					for params in ${paramline[*]}; do
						keyvalues=( ${params//:/ } );
						paramkey=${keyvalues[0]}
						paramvalue=${keyvalues[1]}
#						paramkey=${params//[^a-z0-9]/}
#						paramvalue=${params##*:}
#						debug "paramkey: $paramkey, paramvalue: $paramvalue"

						if [ ! "${params//[^:]/}" = ":" ]; then
							error "$invalidconfigurationtext"
						else
#							debug "*** MEHET ***"
							case $paramkey in
								cargoid)
									if [[ -n $acargoid ]] && [[ $acargoidredefinedcount -eq 0 ]]; then
										warning "$source:$afunctionlinecount: warning: Multiple configuration found for '$paramkey' at AddCargo property!"
										((acargoidredefinedcount++))
									fi
									acargoid="van"
									if [[ -z $addcargocargoids ]]; then
										addcargocargoids="$paramvalue"
									else
										addcargocargoids="$addcargocargoids,$paramvalue"
									fi

									if ! szam16 $paramvalue ; then
										error "$source:$afunctionlinecount: error: Invalid value: '$paramvalue' found for key: 'cargoid' at $functionname function! Allowed only integer values between 0 and 65535!"
									fi
								;;

								action2id)
									if [[ -n $aaction2id ]] && [[ $aaction2idredefinedcount -eq 0 ]]; then
										warning "$source:$afunctionlinecount: warning: Multiple configuration found for '$paramkey' at AddCargo property!"
										((aaction2idredefinedcount++))
									fi
									aaction2id="van"
									if [[ -z $addcargoaction2ids ]]; then
										addcargoaction2ids="$paramvalue"
									else
										addcargoaction2ids="$addcargoaction2ids,$paramvalue"
									fi

									if ! szam16 $paramvalue ; then
										error "$source:$afunctionlinecount: error: Invalid value: '$paramvalue' found for key: 'aaction2id' at $functionname function! Allowed only integer values between 0 and 65535!"
									fi
								;;

								*)
									warning "$source:$afunctionlinecount: warning: Unknown configuration key: '$paramkey' found at AddCargo property!"
								;;
							esac
						fi
 					done

					if [[ -z $acargoid ]]; then
						error "$source:$afunctionlinecount: error: 'cargoid' param is required, but not defined at AddCargo property!"
					fi

					if [[ -z $aaction2id ]]; then
						error "$source:$afunctionlinecount: error: 'action2id' param is required, but not defined at AddCargo property!"
					fi

					((cargoidscount++))
				;;

				Default:*)
					if [[ -z $adddefault ]]; then
						adddefaultfirstline=$afunctionlinecount
					else
						adddefaultline+="$afunctionlinecount,"
						((adddefaultredefinedcount++))
						warning "$source:$adddefaultfirstline: warning: 'Default' property is redefined at line $afunctionlinecount!"
					fi

					if hex $propertyvalue 2; then
						propertyvalue=$(hexraw $propertyvalue)
					elif szam16 $propertyvalue; then
						propertyvalue=$(dectohex $propertyvalue 4)
					else
						error "$source:$afunctionlinecount: error: Invalid value: '$propertyvalue' found for key: 'Default' at $functionname function! Allowed only integer values between 0 and 65535!"
					fi

					adddefault=$propertyvalue
				;;

				*)
					warning "$source:$afunctionlinecount: warning: Invalid function or property!"
				;;
			esac
		fi
	done <<< "$1"



	if [[ -z $stationids ]]; then
		error "$source:$functionlinecount: error: 'StationIDs' property is required, but not defined in this function!"
	elif [[ $stationidsredefinedcount -gt 0 ]]; then
		lines=( ${stationidsline//,/} );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			warning "$source:${lines[$i]}: warning: 'StationIDs' property is already defined at line $stationidsfirstline!"
		done
	fi

	if [[ -z $adddefault ]]; then
		error "$source:$functionlinecount: error: 'Default' property is required, but not defined in this function!"
	elif [[ $adddefaultredefinedcount -gt 0 ]]; then

		lines=( ${adddefaultline//,/} );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			warning "$source:${lines[$i]}: warning: 'Default' property is already defined at line $adddefaultfirstline!"
		done
	fi

	if [[ $addmenuredefinedcount -gt 0 ]]; then
		lines=( ${addmenuline//,/} );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			warning "$source:${lines[$i]}: warning: 'AddMenu' property is already defined at line $addmenufirstline!"
		done
	fi

	if [[ $addcenterredefinedcount -gt 0 ]]; then
		lines=( ${addcenterline//,/} );

		imax=${#lines[@]}
		for ((i=0; i<$imax; i++)); do
			warning "$source:${lines[$i]}: warning: 'AddCenter' property is already defined at line $addcenterfirstline!"
		done
	fi


	if [[ -n $stationids ]] && [[ $stationidscount -gt 0 ]]; then
		bufferedlines="\n   // Action3\n"
		bufferedlines+="   00 * 00\t03 04"
		sc=$(dectohex $stationidscount 2 false)
		bufferedlines+=" $sc"
		bufferedlines+=" $stationids"
		cic=$(dectohex $cargoidscount 2 false)
		bufferedlines+=" $cic"

		if [[ -n $addmenu ]]; then
			bufferedlines+=" FF $addmenu"
		fi

		if [[ -n $addcenter ]]; then
			bufferedlines+=" FE $addcenter"
		fi

		accids=( ${addcargocargoids//,/ } )
		ac2ids=( ${addcargoaction2ids//,/ } )

		imax=${#accids[@]}
		for ((i=0; i<$imax; i++)); do
			acid=${accids[$i]}
			if hex $acid 1; then
				acid=$(hexraw $acid)
			elif szam8 $acid; then
				acid=$(dectohex $acid 2 false)
			else
				error ""
			fi
			bufferedlines+=" $acid"

			a2id=${ac2ids[$i]}
			if hex $a2id 2; then
				a2id=$(hexraw $a2id)
			elif szam8 $a2id; then
				a2id=$(dectohex $a2id 4 false)
			else
				error ""
			fi
			bufferedlines+=" $a2id"
		done

		if [[ -n $adddefault ]]; then
			bufferedlines+=" $adddefault"
		fi

		echo -ne "$bufferedlines" >> "$target"
#		valami=$(echo "$bufferedlines\n" | od -t x1)
#		debug "valami: $valami"
	fi

	echo "$action2autoid"

	if $error ; then returncode=2
	elif $warning ; then returncode=1
	else returncode=0
	fi

	return $returncode
}
