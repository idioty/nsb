#!/bin/bash

#  AddSprite.sh
#  nsb
#
#  Created by Kiss Ferenc on 2014.12.23..
#  Copyright (c) 2014 idioty. All rights reserved.

AssignOneGraphicsSetForAllCargo() {
	error=false
	warning=false

	functionname="AssignOneGraphicsSetForAllCargo"
	functionlinecount="$1"
	action2autoid="$3"

	inaddlayoutfunction=false
	inaddlayoutfunctionlines=""
	inaddlayoutbeginlinecount=0
	inaddlayoutlineparameter=""

	action2id=$action2autoid
	action2ida=""

	stationid=""

	bufferlines=""

	invalidspriteconfigurationtext="$source:$functionlinecount: error: Invalid configuration at $functionname function!"

	functionparameterlines=( ${2//[[:space:]]/} );
	functionparameterlines=( ${functionparameterlines//,/ } );
#	debug "functionparameterlines: ${functionparameterlines[@]}"

	imax=${#functionparameterlines[@]}
	for ((i=0; i<$imax; i++)); do
		functionparameterline="${functionparameterlines[$i]}"
#		debug "functionparameterline: $functionparameterline"
		functionkey=${functionparameterline%:*}
		functionvalue=${functionparameterline##*:}

#		debug "functionkey: $functionkey, functionvalue: $functionvalue"
		case $functionkey in
			action2id)
				if [[ -n $action2ida ]]; then
					warning "$source:$functionlinecount: warning: Multiple configuration found for '$functionkey' at $functionname function!"
				fi

				action2id=$functionvalue
				action2ida="van"
			;;

			stationid)
				if [[ -n $stationid ]]; then
					warning "$source:$functionlinecount: warning: Multiple configuration found for '$functionkey' at $functionname function!"
				fi

				stationid=$functionvalue
			;;

			*)
				warning "$source:$functionlinecount: warning: Unknown configuration ('$functionkey') found at $functionname function!"
			;;
		esac
	done

	if [[ $stationid == "" ]]; then
		error "$source:$functionlinecount: error: 'stationid' property is required, but not defined in this function!"
	else

		if hex $action2id 2; then
			a2id=$(hexraw $action2id)
		elif szam16 $action2id; then
			a2id=$(dectohex $action2id 4)
		else
			error ""
		fi

		if hex $stationid 1; then
			sid=$(hexraw $stationid)
		elif szam8 $stationid; then
			sid=$(dectohex $stationid 2)
		else
			error ""
		fi

#		59 * 7	 03 04 01 02 00 04 00 // Action3, Feature4 (station), used StationIDs: always 1, first (only) stationID, Default Action2ID, 00, azaz
		echo -ne "   00 * 00\t03 04 01 $sid 00 $a2id\n" >> "$target"
	fi

	echo "$action2autoid"

	if $error ; then returncode=2
	elif $warning ; then returncode=1
	else returncode=0
	fi

	return $returncode
}
