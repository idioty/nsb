
AddAction2() {
	error=false
	warning=false

	functionname="AddAction2"
	functionlinecount="$2"
	afunctionlinecount="$2"
	action2autoid="$4"

	infunction=false
	functionreturnlines=""

#	parameterlines=( ${3//,/ } );

	invalidconfigurationtext="$source:$functionlinecount: error: Invalid configuration at $functionname function!"

	action2id=$action2autoid
	action2idredefinedcount=0
	action2ida=""

	adddefault=""
	adddefaultline=""
	adddefaultfirstline=""
	adddefaultredefinedcount=0

	littlecount=0
	addlittlesets=""
	addlittlesetsline=""
	addlittlesetsfirstline=""
	addlittlesetsredefinedcount=0

	lotscount=0
	addlotssets=""
	addlotssetsline=""
	addlotssetsfirstline=""
	addlotssetsredefinedcount=0

	needdefault=true

	parameterlines=( ${3//[[:space:]]/} );
	parameterlines=( ${parameterlines//,/ } );

	imax=${#parameterlines[@]}
	for ((i=0; i<$imax; i++)); do
		parameterline="${parameterlines[$i]}"
#		debug "functionparameterline: $functionparameterline"
		paramkey=${parameterline%:*}
		paramvalue=${parameterline##*:}
#
#		parameterline="${parameterlines[$i]}"
#		params=( ${parameterline//:/ } );
#
#		paramkey=${parameterline%:*}
#		paramvalue=${parameterline##*:}

#		if [[ ! ${parameterline//[^:]/} = ":" ]]; then
		if [[ ${parameterline//[^:]/} = ":" ]]; then
#			debug "van parameter"
#			case $paramkey in
#				continueaction2id)
#					if [[ ! -z $continueaction2id ]] && [[ $continueaction2idredefinedcount -eq 0 ]]; then
#						warning "$source:$functionlinecount: warning: Multiple configuration found for '$paramkey' at $functionname function\n"
#					fi
#
#					action2autoid="$4"
#				;;
#
#				*)
#					error "$invalidstationconfigurationtext"
#				;;
#			esac
#		else
#			debug "key: $paramkey, value: $paramvalue"
			case $paramkey in
				id)
					if [[ ! -z $action2ida ]]; then
						warning "$source:$functionlinecount: warning: Multiple configuration found for $paramkey at $functionname function!"
					fi

					aaction2id=$(dectohex $action2id 2 false)
					action2id=$paramvalue
					action2ida="van"

#
#			// TODO: action2id-t meg kell nezni, hogy hol alakitom at, mert nem talalom, lehet, hogy nincs is auto ertek megadva neki...
#
					if hex $paramvalue 1; then
						paramvalue=$(hexraw $paramvalue)
					elif szam8 $paramvalue; then
						paramvalue=$(dectohex $paramvalue 4)
					else
						error ""
					fi

					if ! [[ $action2id == auto ]]; then
						if ! szam16 $action2id ; then
							error "$source:$inbeginlinecount: error: Invalid value: '$action2id' found for key: 'action2id' at AddAction2 function! Allowed 'auto' or integer value between 0 and 65535!"
						fi
					fi
				;;

#				class)
#				;;

				*)
					warning "$source:$functionlinecount: warning: Unknown configuration ('$paramkey') found at $functionname function!"
				;;
			esac
		fi
	done

	while read -r propertykey
	do
		# delete comments, trim
		propertykey=${propertykey/'#'*/}
		propertykey=${propertykey/\/\/*/}
		propertykey=${propertykey//[[:space:]]/}
		propertyvalue=${propertykey#*:}

		((afunctionlinecount++))
		if [[ $propertykey == @('//'*|'#'*|'') ]]; then
			if $infunction; then
				infunctionlines+="emptyline"$'\n'
			fi
		elif [[ $propertykey == @EndAddVariationalAction2 ]]; then
			functionreturnlines+=$(AddVariationalAction2 "$infunctionlines" "$inbeginlinecount" "$aparamters")
			sfrc=$?
			case $sfrc in
				1) warning=true ;;
				2) error=true ;;
			esac

			infunction=false
			infunctionlines=""
			inbeginlinecount=0
		elif [[ $propertykey == @EndAddRandomAction2 ]]; then
			functionreturnlines+=$(AddRandomAction2 "$infunctionlines" "$inbeginlinecount" "$aparamters")
			sfrc=$?
			case $sfrc in
				1) warning=true ;;
				2) error=true ;;
			esac

			infunction=false
			infunctionlines=""
			inbeginlinecount=0
		elif $infunction; then
			infunctionlines+="$propertykey"$'\n'
		else
			case $propertykey in
				Default:*)
					if [[ -z $adddefault ]]; then
						adddefaultfirstline=$afunctionlinecount
					else
						adddefaultline+="$afunctionlinecount,"
						((adddefaultredefinedcount++))
						warning "$source:$adddefaultfirstline: warning: 'Default' property is redefined at line $afunctionlinecount!"
					fi

					if hex $propertyvalue 2; then
						propertyvalue=$(hexraw $propertyvalue)
					elif szam16 $propertyvalue; then
						propertyvalue=$(dectohex $propertyvalue 4)
					else
						error "$source:$afunctionlinecount: error: Invalid value: '$propertyvalue' found for key: 'Default' at $functionname function! Allowed only integer values between 0 and 65535!"
					fi

					adddefault=$propertyvalue
				;;

				LittleSets:*)
					if [[ -z $addlittlesets ]]; then
						addlittlesetsfirstline=$afunctionlinecount
					else
						addlittlesetsline+="$afunctionlinecount,"
						((addlittlesetsredefinedcount++))
						warning "$source:$addlittlesetsfirstline: warning: 'LittleSets' property is redefined at line $afunctionlinecount!"
					fi

					addlittlesets=$propertyvalue
				;;

				LotsSets:*)
					if [[ -z $addlotssets ]]; then
						addlotssetsfirstline=$afunctionlinecount
					else
						addlotssetsline+="$afunctionlinecount,"
						((addlotssetsredefinedcount++))
						warning "$source:$addlotssetsfirstline: warning: 'LotsSets' property is redefined at line $afunctionlinecount!"
					fi

					addlotssets=$propertyvalue
				;;

				@AddVariationalAction2:*)
					aparamters=$propertyvalue
					infunction=true
					infunctionlines=""
					inbeginlinecount=$afunctionlinecount
				;;

				@AddRandomAction2*)
					aparamters=$propertyvalue
					infunction=true
					infunctionlines=""
					inbeginlinecount=$afunctionlinecount
					needdefault=false
				;;

				*)
					warning "$source:$afunctionlinecount: warning: Invalid function or property!"
				;;
			esac
		fi
	done <<< "$1"

	# ide jon a megvalositas
#	aaction2id=$(dectohex $action2id 2 false)
	buff="\n\t//Action2\n   00 * 00\t02 04 $action2id\n"

	if [[ -n $addlittlesets ]] && [[ -n $addlotssets ]]; then
#		debug "addlittlesets: $addlittlesets"
#		debug "addlotssets: $addlotssets"
		aliclines=( ${addlittlesets//,/ } );
		aloclines=( ${addlotssets//,/ } );
#		debug "aliclines: ${aliclines[@]}"
#		debug "aloclines: ${aloclines[@]}"
		alic=$(dectohex ${#aliclines[@]}  2)
		aloc=$(dectohex ${#aloclines[@]}  2)

		buff+="\t\t\t$alic $aloc\t\t\t//littlesets count, lotssets count\n"

		buff+="\t\t\t"
		imax=${#aliclines[@]}
		for ((i=0; i<$imax; i++)); do
			szam="${aliclines[$i]}"
			if hex $szam 2; then
				szam=$(hexraw $szam)
			elif szam16 $szam; then
				szam=$(dectohex $szam 4 true spaces)
			else
				error "$source:$addlittlesetsfirstline: error: Invalid value: '$szam' found for key: 'LittleSets' at $functionname function! Allowed only integer values between 0 and 65535!"
			fi
			buff+="$szam"
			if [[ $i -lt $imax-1 ]]; then
				buff+=" "
			fi
		done
		buff+="\t\t\t//LittleSets\n"

		buff+="\t\t\t"
		imax=${#aloclines[@]}
		for ((i=0; i<$imax; i++)); do
			szam="${aloclines[$i]}"
			if hex $szam 2; then
				szam=$(hexraw $szam)
			elif szam16 $szam; then
				szam=$(dectohex $szam 4 true spaces)
			else
				error "$source:$addlotssetsfirstline: error: Invalid value: '$szam' found for key: 'LotsSets' at $functionname function! Allowed only integer values between 0 and 65535!"
			fi
			buff+="$szam"
			if [[ $i -lt $imax-1 ]]; then
				buff+=" "
			fi
		done
		buff+="\t\t\t//LotsSets\n"
	else
		if $needdefault; then
			if [[ -z $adddefault ]]; then
				error "$source:$functionlinecount: error: 'Default' property is required, but not defined in this function!"
			elif [[ $adddefaultredefinedcount -gt 0 ]]; then
				lines=( ${adddefaultline//,/} );

				imax=${#lines[@]}
				for ((i=0; i<$imax; i++)); do
					warning "$source:${lines[$i]}: warning: 'Default' property is already defined at line $adddefaultfirstline!"
				done
			fi
		fi

		buff+="\t\t\t$functionreturnlines"
		if $needdefault; then
			buff+="\t\t\t$adddefault\t\t\t//default action2id"
		fi
	fi

	echo -e "$buff" >> "$target"

	if [[ $action2id -eq $action2autoid ]]; then
		((action2autoid++))
	fi

	echo "$action2autoid"

	if $error ; then returncode=2
	elif $warning ; then returncode=1
	else returncode=0
	fi

	return $returncode
}